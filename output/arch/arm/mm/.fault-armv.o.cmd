cmd_arch/arm/mm/fault-armv.o := /home/michael/kernel/Kernel/scripts/gcc-wrapper.py /home/michael/kernel/toolchain/arm-eabi-4.8/bin/arm-eabi-gcc -Wp,-MD,arch/arm/mm/.fault-armv.o.d  -nostdinc -isystem /home/michael/kernel/toolchain/arm-eabi-4.8/bin/../lib/gcc/arm-eabi/4.8.5/include -I/home/michael/kernel/Kernel/arch/arm/include -Iarch/arm/include/generated -Iinclude  -I/home/michael/kernel/Kernel/include -include /home/michael/kernel/Kernel/include/linux/kconfig.h  -I/home/michael/kernel/Kernel/arch/arm/mm -Iarch/arm/mm -D__KERNEL__ -mlittle-endian   -I/home/michael/kernel/Kernel/arch/arm/mach-msm/include -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -Wno-maybe-uninitialized -marm -fno-dwarf2-cfi-asm -fstack-protector -mabi=aapcs-linux -mno-thumb-interwork -funwind-tables -D__LINUX_ARM_ARCH__=7 -mcpu=cortex-a15 -msoft-float -Uarm -Wframe-larger-than=1024 -Wno-unused-but-set-variable -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(fault_armv)"  -D"KBUILD_MODNAME=KBUILD_STR(fault_armv)" -c -o arch/arm/mm/fault-armv.o /home/michael/kernel/Kernel/arch/arm/mm/fault-armv.c

source_arch/arm/mm/fault-armv.o := /home/michael/kernel/Kernel/arch/arm/mm/fault-armv.c

deps_arch/arm/mm/fault-armv.o := \
    $(wildcard include/config/cpu/cache/vipt.h) \
  /home/michael/kernel/Kernel/include/linux/sched.h \
    $(wildcard include/config/runtime/compcache.h) \
    $(wildcard include/config/sched/debug.h) \
    $(wildcard include/config/prove/rcu.h) \
    $(wildcard include/config/smp.h) \
    $(wildcard include/config/no/hz.h) \
    $(wildcard include/config/lockup/detector.h) \
    $(wildcard include/config/detect/hung/task.h) \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/core/dump/default/elf/headers.h) \
    $(wildcard include/config/sched/autogroup.h) \
    $(wildcard include/config/virt/cpu/accounting.h) \
    $(wildcard include/config/bsd/process/acct.h) \
    $(wildcard include/config/taskstats.h) \
    $(wildcard include/config/audit.h) \
    $(wildcard include/config/cgroups.h) \
    $(wildcard include/config/samp/hotness.h) \
    $(wildcard include/config/inotify/user.h) \
    $(wildcard include/config/fanotify.h) \
    $(wildcard include/config/epoll.h) \
    $(wildcard include/config/posix/mqueue.h) \
    $(wildcard include/config/keys.h) \
    $(wildcard include/config/perf/events.h) \
    $(wildcard include/config/schedstats.h) \
    $(wildcard include/config/task/delay/acct.h) \
    $(wildcard include/config/fair/group/sched.h) \
    $(wildcard include/config/rt/group/sched.h) \
    $(wildcard include/config/preempt/notifiers.h) \
    $(wildcard include/config/blk/dev/io/trace.h) \
    $(wildcard include/config/preempt/rcu.h) \
    $(wildcard include/config/tree/preempt/rcu.h) \
    $(wildcard include/config/rcu/boost.h) \
    $(wildcard include/config/compat/brk.h) \
    $(wildcard include/config/generic/hardirqs.h) \
    $(wildcard include/config/cc/stackprotector.h) \
    $(wildcard include/config/sysvipc.h) \
    $(wildcard include/config/auditsyscall.h) \
    $(wildcard include/config/rt/mutexes.h) \
    $(wildcard include/config/debug/mutexes.h) \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/lockdep.h) \
    $(wildcard include/config/block.h) \
    $(wildcard include/config/task/xacct.h) \
    $(wildcard include/config/cpusets.h) \
    $(wildcard include/config/futex.h) \
    $(wildcard include/config/compat.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/fault/injection.h) \
    $(wildcard include/config/latencytop.h) \
    $(wildcard include/config/function/graph/tracer.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/cgroup/mem/res/ctlr.h) \
    $(wildcard include/config/have/hw/breakpoint.h) \
    $(wildcard include/config/sdp.h) \
    $(wildcard include/config/cpumask/offstack.h) \
    $(wildcard include/config/have/unstable/sched/clock.h) \
    $(wildcard include/config/irq/time/accounting.h) \
    $(wildcard include/config/hotplug/cpu.h) \
    $(wildcard include/config/proc/fs.h) \
    $(wildcard include/config/cfs/bandwidth.h) \
    $(wildcard include/config/stack/growsup.h) \
    $(wildcard include/config/debug/stack/usage.h) \
    $(wildcard include/config/preempt/count.h) \
    $(wildcard include/config/preempt.h) \
    $(wildcard include/config/cgroup/sched.h) \
    $(wildcard include/config/mm/owner.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/param.h \
    $(wildcard include/config/hz.h) \
  /home/michael/kernel/Kernel/include/linux/capability.h \
  /home/michael/kernel/Kernel/include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/types.h \
  /home/michael/kernel/Kernel/include/asm-generic/int-ll64.h \
  arch/arm/include/generated/asm/bitsperlong.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitsperlong.h \
  /home/michael/kernel/Kernel/include/linux/posix_types.h \
  /home/michael/kernel/Kernel/include/linux/stddef.h \
  /home/michael/kernel/Kernel/include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc4.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/posix_types.h \
  /home/michael/kernel/Kernel/include/asm-generic/posix_types.h \
  /home/michael/kernel/Kernel/include/linux/threads.h \
    $(wildcard include/config/nr/cpus.h) \
    $(wildcard include/config/base/small.h) \
  /home/michael/kernel/Kernel/include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/ring/buffer.h) \
    $(wildcard include/config/compaction.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
  /home/michael/kernel/Kernel/include/linux/sysinfo.h \
  /home/michael/kernel/toolchain/arm-eabi-4.8/lib/gcc/arm-eabi/4.8.5/include/stdarg.h \
  /home/michael/kernel/Kernel/include/linux/linkage.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/linkage.h \
  /home/michael/kernel/Kernel/include/linux/bitops.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bitops.h \
  /home/michael/kernel/Kernel/include/linux/irqflags.h \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /home/michael/kernel/Kernel/include/linux/typecheck.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/irqflags.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/cpu/endian/be8.h) \
    $(wildcard include/config/arm/thumb.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hwcap.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/non-atomic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/fls64.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/sched.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/arch_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/const_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/lock.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/le.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/byteorder.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/little_endian.h \
  /home/michael/kernel/Kernel/include/linux/swab.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/swab.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/generic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /home/michael/kernel/Kernel/include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  /home/michael/kernel/Kernel/include/linux/printk.h \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
    $(wildcard include/config/gsm/modem/sprd6500.h) \
  /home/michael/kernel/Kernel/include/linux/init.h \
    $(wildcard include/config/deferred/initcalls.h) \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/hotplug.h) \
  /home/michael/kernel/Kernel/include/linux/dynamic_debug.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/div64.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/compiler.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/thumb2/kernel.h) \
    $(wildcard include/config/debug/bugverbose.h) \
    $(wildcard include/config/arm/lpae.h) \
  /home/michael/kernel/Kernel/include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /home/michael/kernel/Kernel/include/linux/timex.h \
  /home/michael/kernel/Kernel/include/linux/time.h \
    $(wildcard include/config/arch/uses/gettimeoffset.h) \
  /home/michael/kernel/Kernel/include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/cache.h \
    $(wildcard include/config/arm/l1/cache/shift.h) \
    $(wildcard include/config/aeabi.h) \
  /home/michael/kernel/Kernel/include/linux/seqlock.h \
  /home/michael/kernel/Kernel/include/linux/spinlock.h \
    $(wildcard include/config/debug/spinlock.h) \
    $(wildcard include/config/generic/lockbreak.h) \
    $(wildcard include/config/debug/lock/alloc.h) \
  /home/michael/kernel/Kernel/include/linux/preempt.h \
    $(wildcard include/config/debug/preempt.h) \
  /home/michael/kernel/Kernel/include/linux/thread_info.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/thread_info.h \
    $(wildcard include/config/arm/thumbee.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/fpstate.h \
    $(wildcard include/config/vfpv3.h) \
    $(wildcard include/config/iwmmxt.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/domain.h \
    $(wildcard include/config/verify/permission/fault.h) \
    $(wildcard include/config/io/36.h) \
    $(wildcard include/config/cpu/use/domains.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/barrier.h \
    $(wildcard include/config/cpu/32v6k.h) \
    $(wildcard include/config/cpu/xsc3.h) \
    $(wildcard include/config/cpu/fa526.h) \
    $(wildcard include/config/arch/has/barriers.h) \
    $(wildcard include/config/arm/dma/mem/bufferable.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/outercache.h \
    $(wildcard include/config/outer/cache/sync.h) \
    $(wildcard include/config/outer/cache.h) \
  /home/michael/kernel/Kernel/include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  /home/michael/kernel/Kernel/include/linux/poison.h \
    $(wildcard include/config/illegal/pointer/value.h) \
  /home/michael/kernel/Kernel/include/linux/const.h \
  /home/michael/kernel/Kernel/include/linux/stringify.h \
  /home/michael/kernel/Kernel/include/linux/bottom_half.h \
  /home/michael/kernel/Kernel/include/linux/spinlock_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/spinlock_types.h \
  /home/michael/kernel/Kernel/include/linux/lockdep.h \
    $(wildcard include/config/lock/stat.h) \
  /home/michael/kernel/Kernel/include/linux/rwlock_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/spinlock.h \
    $(wildcard include/config/msm/krait/wfe/fixup.h) \
    $(wildcard include/config/arm/ticket/locks.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/processor.h \
    $(wildcard include/config/arm/errata/754327.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hw_breakpoint.h \
  /home/michael/kernel/Kernel/include/linux/rwlock.h \
  /home/michael/kernel/Kernel/include/linux/spinlock_api_smp.h \
    $(wildcard include/config/inline/spin/lock.h) \
    $(wildcard include/config/inline/spin/lock/bh.h) \
    $(wildcard include/config/inline/spin/lock/irq.h) \
    $(wildcard include/config/inline/spin/lock/irqsave.h) \
    $(wildcard include/config/inline/spin/trylock.h) \
    $(wildcard include/config/inline/spin/trylock/bh.h) \
    $(wildcard include/config/uninline/spin/unlock.h) \
    $(wildcard include/config/inline/spin/unlock/bh.h) \
    $(wildcard include/config/inline/spin/unlock/irq.h) \
    $(wildcard include/config/inline/spin/unlock/irqrestore.h) \
  /home/michael/kernel/Kernel/include/linux/rwlock_api_smp.h \
    $(wildcard include/config/inline/read/lock.h) \
    $(wildcard include/config/inline/write/lock.h) \
    $(wildcard include/config/inline/read/lock/bh.h) \
    $(wildcard include/config/inline/write/lock/bh.h) \
    $(wildcard include/config/inline/read/lock/irq.h) \
    $(wildcard include/config/inline/write/lock/irq.h) \
    $(wildcard include/config/inline/read/lock/irqsave.h) \
    $(wildcard include/config/inline/write/lock/irqsave.h) \
    $(wildcard include/config/inline/read/trylock.h) \
    $(wildcard include/config/inline/write/trylock.h) \
    $(wildcard include/config/inline/read/unlock.h) \
    $(wildcard include/config/inline/write/unlock.h) \
    $(wildcard include/config/inline/read/unlock/bh.h) \
    $(wildcard include/config/inline/write/unlock/bh.h) \
    $(wildcard include/config/inline/read/unlock/irq.h) \
    $(wildcard include/config/inline/write/unlock/irq.h) \
    $(wildcard include/config/inline/read/unlock/irqrestore.h) \
    $(wildcard include/config/inline/write/unlock/irqrestore.h) \
  /home/michael/kernel/Kernel/include/linux/atomic.h \
    $(wildcard include/config/arch/has/atomic/or.h) \
    $(wildcard include/config/generic/atomic64.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/atomic.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/cmpxchg.h \
    $(wildcard include/config/cpu/sa1100.h) \
    $(wildcard include/config/cpu/sa110.h) \
    $(wildcard include/config/cpu/v6.h) \
  /home/michael/kernel/Kernel/include/asm-generic/cmpxchg-local.h \
  /home/michael/kernel/Kernel/include/asm-generic/atomic-long.h \
  /home/michael/kernel/Kernel/include/linux/math64.h \
  /home/michael/kernel/Kernel/include/linux/param.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/timex.h \
  /home/michael/kernel/Kernel/arch/arm/mach-msm/include/mach/timex.h \
    $(wildcard include/config/have/arch/has/current/timer.h) \
  /home/michael/kernel/Kernel/include/linux/jiffies.h \
  /home/michael/kernel/Kernel/include/linux/rbtree.h \
  /home/michael/kernel/Kernel/include/linux/cpumask.h \
    $(wildcard include/config/debug/per/cpu/maps.h) \
    $(wildcard include/config/disable/obsolete/cpumask/functions.h) \
  /home/michael/kernel/Kernel/include/linux/bitmap.h \
  /home/michael/kernel/Kernel/include/linux/string.h \
    $(wildcard include/config/binary/printf.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/string.h \
  /home/michael/kernel/Kernel/include/linux/bug.h \
    $(wildcard include/config/panic/on/data/corruption.h) \
  /home/michael/kernel/Kernel/include/linux/errno.h \
  arch/arm/include/generated/asm/errno.h \
  /home/michael/kernel/Kernel/include/asm-generic/errno.h \
  /home/michael/kernel/Kernel/include/asm-generic/errno-base.h \
  /home/michael/kernel/Kernel/include/linux/nodemask.h \
    $(wildcard include/config/highmem.h) \
  /home/michael/kernel/Kernel/include/linux/numa.h \
    $(wildcard include/config/nodes/shift.h) \
  /home/michael/kernel/Kernel/include/linux/mm_types.h \
    $(wildcard include/config/split/ptlock/cpus.h) \
    $(wildcard include/config/want/page/debug/flags.h) \
    $(wildcard include/config/kmemcheck.h) \
    $(wildcard include/config/have/aligned/struct/page.h) \
    $(wildcard include/config/aio.h) \
    $(wildcard include/config/mmu/notifier.h) \
    $(wildcard include/config/transparent/hugepage.h) \
  /home/michael/kernel/Kernel/include/linux/auxvec.h \
  arch/arm/include/generated/asm/auxvec.h \
  /home/michael/kernel/Kernel/include/asm-generic/auxvec.h \
  /home/michael/kernel/Kernel/include/linux/prio_tree.h \
  /home/michael/kernel/Kernel/include/linux/rwsem.h \
    $(wildcard include/config/rwsem/generic/spinlock.h) \
  /home/michael/kernel/Kernel/include/linux/rwsem-spinlock.h \
    $(wildcard include/config/sec/forkhang/debug.h) \
  /home/michael/kernel/Kernel/include/linux/completion.h \
  /home/michael/kernel/Kernel/include/linux/wait.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/current.h \
  /home/michael/kernel/Kernel/include/linux/page-debug-flags.h \
    $(wildcard include/config/page/poisoning.h) \
    $(wildcard include/config/page/guard.h) \
    $(wildcard include/config/page/debug/something/else.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/page.h \
    $(wildcard include/config/cpu/copy/v3.h) \
    $(wildcard include/config/cpu/copy/v4wt.h) \
    $(wildcard include/config/cpu/copy/v4wb.h) \
    $(wildcard include/config/cpu/copy/feroceon.h) \
    $(wildcard include/config/cpu/copy/fa.h) \
    $(wildcard include/config/cpu/xscale.h) \
    $(wildcard include/config/cpu/copy/v6.h) \
    $(wildcard include/config/kuser/helpers.h) \
    $(wildcard include/config/have/arch/pfn/valid.h) \
    $(wildcard include/config/memory/hotplug/sparse.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/glue.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-2level-types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/memory.h \
    $(wildcard include/config/need/mach/memory/h.h) \
    $(wildcard include/config/page/offset.h) \
    $(wildcard include/config/dram/size.h) \
    $(wildcard include/config/dram/base.h) \
    $(wildcard include/config/have/tcm.h) \
    $(wildcard include/config/arm/patch/phys/virt.h) \
    $(wildcard include/config/phys/offset.h) \
  arch/arm/include/generated/asm/sizes.h \
  /home/michael/kernel/Kernel/include/asm-generic/sizes.h \
  /home/michael/kernel/Kernel/arch/arm/mach-msm/include/mach/memory.h \
    $(wildcard include/config/arch/msm7x30.h) \
    $(wildcard include/config/sparsemem.h) \
    $(wildcard include/config/vmsplit/3g.h) \
    $(wildcard include/config/arch/msm/arm11.h) \
    $(wildcard include/config/arch/msm/cortex/a5.h) \
    $(wildcard include/config/cache/l2x0.h) \
    $(wildcard include/config/arch/msm8x60.h) \
    $(wildcard include/config/arch/msm8960.h) \
    $(wildcard include/config/dont/map/hole/after/membank0.h) \
    $(wildcard include/config/arch/msm/scorpion.h) \
    $(wildcard include/config/arch/msm/krait.h) \
    $(wildcard include/config/arch/msm7x27.h) \
  /home/michael/kernel/Kernel/include/asm-generic/memory_model.h \
    $(wildcard include/config/flatmem.h) \
    $(wildcard include/config/discontigmem.h) \
    $(wildcard include/config/sparsemem/vmemmap.h) \
  /home/michael/kernel/Kernel/include/asm-generic/getorder.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/mmu.h \
    $(wildcard include/config/cpu/has/asid.h) \
  arch/arm/include/generated/asm/cputime.h \
  /home/michael/kernel/Kernel/include/asm-generic/cputime.h \
  /home/michael/kernel/Kernel/include/linux/smp.h \
    $(wildcard include/config/use/generic/smp/helpers.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/smp.h \
  /home/michael/kernel/Kernel/include/linux/sem.h \
  /home/michael/kernel/Kernel/include/linux/ipc.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/ipcbuf.h \
  /home/michael/kernel/Kernel/include/asm-generic/ipcbuf.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/sembuf.h \
  /home/michael/kernel/Kernel/include/linux/rcupdate.h \
    $(wildcard include/config/rcu/torture/test.h) \
    $(wildcard include/config/tree/rcu.h) \
    $(wildcard include/config/rcu/trace.h) \
    $(wildcard include/config/tiny/rcu.h) \
    $(wildcard include/config/tiny/preempt/rcu.h) \
    $(wildcard include/config/debug/objects/rcu/head.h) \
    $(wildcard include/config/preempt/rt.h) \
  /home/michael/kernel/Kernel/include/linux/debugobjects.h \
    $(wildcard include/config/debug/objects.h) \
    $(wildcard include/config/debug/objects/free.h) \
  /home/michael/kernel/Kernel/include/linux/rcutree.h \
  /home/michael/kernel/Kernel/include/linux/signal.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/signal.h \
  /home/michael/kernel/Kernel/include/asm-generic/signal-defs.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/sigcontext.h \
  arch/arm/include/generated/asm/siginfo.h \
  /home/michael/kernel/Kernel/include/asm-generic/siginfo.h \
  /home/michael/kernel/Kernel/include/linux/pid.h \
  /home/michael/kernel/Kernel/include/linux/percpu.h \
    $(wildcard include/config/need/per/cpu/embed/first/chunk.h) \
    $(wildcard include/config/need/per/cpu/page/first/chunk.h) \
    $(wildcard include/config/have/setup/per/cpu/area.h) \
  /home/michael/kernel/Kernel/include/linux/pfn.h \
  arch/arm/include/generated/asm/percpu.h \
  /home/michael/kernel/Kernel/include/asm-generic/percpu.h \
  /home/michael/kernel/Kernel/include/linux/percpu-defs.h \
    $(wildcard include/config/debug/force/weak/per/cpu.h) \
  /home/michael/kernel/Kernel/include/linux/topology.h \
    $(wildcard include/config/sched/smt.h) \
    $(wildcard include/config/sched/mc.h) \
    $(wildcard include/config/sched/book.h) \
    $(wildcard include/config/use/percpu/numa/node/id.h) \
    $(wildcard include/config/have/memoryless/nodes.h) \
  /home/michael/kernel/Kernel/include/linux/mmzone.h \
    $(wildcard include/config/force/max/zoneorder.h) \
    $(wildcard include/config/cma.h) \
    $(wildcard include/config/cma/page/counting.h) \
    $(wildcard include/config/zone/dma.h) \
    $(wildcard include/config/zone/dma32.h) \
    $(wildcard include/config/memory/hotplug.h) \
    $(wildcard include/config/have/memblock/node/map.h) \
    $(wildcard include/config/flat/node/mem/map.h) \
    $(wildcard include/config/no/bootmem.h) \
    $(wildcard include/config/have/memory/present.h) \
    $(wildcard include/config/need/node/memmap/size.h) \
    $(wildcard include/config/have/memblock/node.h) \
    $(wildcard include/config/need/multiple/nodes.h) \
    $(wildcard include/config/have/arch/early/pfn/to/nid.h) \
    $(wildcard include/config/sparsemem/extreme.h) \
    $(wildcard include/config/nodes/span/other/nodes.h) \
    $(wildcard include/config/holes/in/zone.h) \
    $(wildcard include/config/arch/has/holes/memorymodel.h) \
  /home/michael/kernel/Kernel/include/linux/pageblock-flags.h \
    $(wildcard include/config/hugetlb/page.h) \
    $(wildcard include/config/hugetlb/page/size/variable.h) \
  include/generated/bounds.h \
  /home/michael/kernel/Kernel/include/linux/memory_hotplug.h \
    $(wildcard include/config/memory/hotremove.h) \
    $(wildcard include/config/have/arch/nodedata/extension.h) \
  /home/michael/kernel/Kernel/include/linux/notifier.h \
  /home/michael/kernel/Kernel/include/linux/mutex.h \
    $(wildcard include/config/arch/msm8610.h) \
    $(wildcard include/config/have/arch/mutex/cpu/relax.h) \
  /home/michael/kernel/Kernel/include/linux/srcu.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/topology.h \
    $(wildcard include/config/arm/cpu/topology.h) \
  /home/michael/kernel/Kernel/include/asm-generic/topology.h \
  /home/michael/kernel/Kernel/include/linux/proportions.h \
  /home/michael/kernel/Kernel/include/linux/percpu_counter.h \
  /home/michael/kernel/Kernel/include/linux/seccomp.h \
    $(wildcard include/config/seccomp.h) \
  /home/michael/kernel/Kernel/include/linux/rculist.h \
  /home/michael/kernel/Kernel/include/linux/rtmutex.h \
    $(wildcard include/config/debug/rt/mutexes.h) \
  /home/michael/kernel/Kernel/include/linux/plist.h \
    $(wildcard include/config/debug/pi/list.h) \
  /home/michael/kernel/Kernel/include/linux/resource.h \
  arch/arm/include/generated/asm/resource.h \
  /home/michael/kernel/Kernel/include/asm-generic/resource.h \
  /home/michael/kernel/Kernel/include/linux/timer.h \
    $(wildcard include/config/timer/stats.h) \
    $(wildcard include/config/debug/objects/timers.h) \
  /home/michael/kernel/Kernel/include/linux/ktime.h \
    $(wildcard include/config/ktime/scalar.h) \
  /home/michael/kernel/Kernel/include/linux/hrtimer.h \
    $(wildcard include/config/high/res/timers.h) \
    $(wildcard include/config/timerfd.h) \
  /home/michael/kernel/Kernel/include/linux/timerqueue.h \
  /home/michael/kernel/Kernel/include/linux/task_io_accounting.h \
    $(wildcard include/config/task/io/accounting.h) \
  /home/michael/kernel/Kernel/include/linux/latencytop.h \
  /home/michael/kernel/Kernel/include/linux/cred.h \
    $(wildcard include/config/debug/credentials.h) \
    $(wildcard include/config/security.h) \
    $(wildcard include/config/user/ns.h) \
  /home/michael/kernel/Kernel/include/linux/key.h \
    $(wildcard include/config/sysctl.h) \
  /home/michael/kernel/Kernel/include/linux/sysctl.h \
  /home/michael/kernel/Kernel/include/linux/selinux.h \
    $(wildcard include/config/security/selinux.h) \
  /home/michael/kernel/Kernel/include/linux/llist.h \
    $(wildcard include/config/arch/have/nmi/safe/cmpxchg.h) \
  /home/michael/kernel/Kernel/include/linux/aio.h \
  /home/michael/kernel/Kernel/include/linux/workqueue.h \
    $(wildcard include/config/debug/objects/work.h) \
    $(wildcard include/config/freezer.h) \
    $(wildcard include/config/workqueue/front.h) \
  /home/michael/kernel/Kernel/include/linux/aio_abi.h \
  /home/michael/kernel/Kernel/include/linux/uio.h \
  /home/michael/kernel/Kernel/include/linux/mm.h \
    $(wildcard include/config/fix/movable/zone.h) \
    $(wildcard include/config/ia64.h) \
    $(wildcard include/config/ksm.h) \
    $(wildcard include/config/debug/pagealloc.h) \
    $(wildcard include/config/hibernation.h) \
    $(wildcard include/config/use/user/accessible/timers.h) \
    $(wildcard include/config/hugetlbfs.h) \
  /home/michael/kernel/Kernel/include/linux/gfp.h \
    $(wildcard include/config/pm/sleep.h) \
  /home/michael/kernel/Kernel/include/linux/mmdebug.h \
    $(wildcard include/config/debug/vm.h) \
    $(wildcard include/config/debug/virtual.h) \
  /home/michael/kernel/Kernel/include/linux/debug_locks.h \
    $(wildcard include/config/debug/locking/api/selftests.h) \
  /home/michael/kernel/Kernel/include/linux/range.h \
  /home/michael/kernel/Kernel/include/linux/bit_spinlock.h \
  /home/michael/kernel/Kernel/include/linux/shrinker.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable.h \
    $(wildcard include/config/highpte.h) \
    $(wildcard include/config/tima/rkp/l2/group.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/proc-fns.h \
    $(wildcard include/config/tima/rkp/l2/tables.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/glue-proc.h \
    $(wildcard include/config/cpu/arm610.h) \
    $(wildcard include/config/cpu/arm7tdmi.h) \
    $(wildcard include/config/cpu/arm710.h) \
    $(wildcard include/config/cpu/arm720t.h) \
    $(wildcard include/config/cpu/arm740t.h) \
    $(wildcard include/config/cpu/arm9tdmi.h) \
    $(wildcard include/config/cpu/arm920t.h) \
    $(wildcard include/config/cpu/arm922t.h) \
    $(wildcard include/config/cpu/arm925t.h) \
    $(wildcard include/config/cpu/arm926t.h) \
    $(wildcard include/config/cpu/arm940t.h) \
    $(wildcard include/config/cpu/arm946e.h) \
    $(wildcard include/config/cpu/arm1020.h) \
    $(wildcard include/config/cpu/arm1020e.h) \
    $(wildcard include/config/cpu/arm1022.h) \
    $(wildcard include/config/cpu/arm1026.h) \
    $(wildcard include/config/cpu/mohawk.h) \
    $(wildcard include/config/cpu/feroceon.h) \
    $(wildcard include/config/cpu/v6k.h) \
    $(wildcard include/config/cpu/v7.h) \
  /home/michael/kernel/Kernel/include/asm-generic/pgtable-nopud.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-hwdef.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-2level-hwdef.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-2level.h \
    $(wildcard include/config/tima/rkp.h) \
    $(wildcard include/config/tima/rkp/l1/tables.h) \
    $(wildcard include/config/tima/rkp/lazy/mmu.h) \
  /home/michael/kernel/Kernel/include/asm-generic/pgtable.h \
  /home/michael/kernel/Kernel/include/linux/slab.h \
    $(wildcard include/config/slab/debug.h) \
    $(wildcard include/config/failslab.h) \
    $(wildcard include/config/slub.h) \
    $(wildcard include/config/slob.h) \
    $(wildcard include/config/debug/slab.h) \
    $(wildcard include/config/slab.h) \
  /home/michael/kernel/Kernel/include/linux/slub_def.h \
    $(wildcard include/config/slub/stats.h) \
    $(wildcard include/config/slub/debug.h) \
    $(wildcard include/config/sysfs.h) \
  /home/michael/kernel/Kernel/include/linux/kobject.h \
  /home/michael/kernel/Kernel/include/linux/sysfs.h \
  /home/michael/kernel/Kernel/include/linux/kobject_ns.h \
  /home/michael/kernel/Kernel/include/linux/kref.h \
  /home/michael/kernel/Kernel/include/linux/kmemleak.h \
    $(wildcard include/config/debug/kmemleak.h) \
  /home/michael/kernel/Kernel/include/linux/page-flags.h \
    $(wildcard include/config/pageflags/extended.h) \
    $(wildcard include/config/arch/uses/pg/uncached.h) \
    $(wildcard include/config/memory/failure.h) \
    $(wildcard include/config/scfs/lower/pagecache/invalidation.h) \
    $(wildcard include/config/swap.h) \
    $(wildcard include/config/s390.h) \
  /home/michael/kernel/Kernel/include/linux/huge_mm.h \
  /home/michael/kernel/Kernel/include/linux/vmstat.h \
    $(wildcard include/config/vm/event/counters.h) \
  /home/michael/kernel/Kernel/include/linux/vm_event_item.h \
    $(wildcard include/config/migration.h) \
  /home/michael/kernel/Kernel/include/linux/vmalloc.h \
    $(wildcard include/config/enable/vmalloc/saving.h) \
  /home/michael/kernel/Kernel/include/linux/pagemap.h \
  /home/michael/kernel/Kernel/include/linux/fs.h \
    $(wildcard include/config/fs/posix/acl.h) \
    $(wildcard include/config/quota.h) \
    $(wildcard include/config/fsnotify.h) \
    $(wildcard include/config/ima.h) \
    $(wildcard include/config/debug/writecount.h) \
    $(wildcard include/config/file/locking.h) \
    $(wildcard include/config/fs/xip.h) \
  /home/michael/kernel/Kernel/include/linux/limits.h \
  /home/michael/kernel/Kernel/include/linux/ioctl.h \
  arch/arm/include/generated/asm/ioctl.h \
  /home/michael/kernel/Kernel/include/asm-generic/ioctl.h \
  /home/michael/kernel/Kernel/include/linux/blk_types.h \
    $(wildcard include/config/blk/dev/integrity.h) \
  /home/michael/kernel/Kernel/include/linux/kdev_t.h \
  /home/michael/kernel/Kernel/include/linux/dcache.h \
  /home/michael/kernel/Kernel/include/linux/rculist_bl.h \
  /home/michael/kernel/Kernel/include/linux/list_bl.h \
  /home/michael/kernel/Kernel/include/linux/path.h \
  /home/michael/kernel/Kernel/include/linux/stat.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/stat.h \
  /home/michael/kernel/Kernel/include/linux/radix-tree.h \
  /home/michael/kernel/Kernel/include/linux/semaphore.h \
  /home/michael/kernel/Kernel/include/linux/fiemap.h \
  /home/michael/kernel/Kernel/include/linux/migrate_mode.h \
  /home/michael/kernel/Kernel/include/linux/quota.h \
    $(wildcard include/config/quota/netlink/interface.h) \
  /home/michael/kernel/Kernel/include/linux/dqblk_xfs.h \
  /home/michael/kernel/Kernel/include/linux/dqblk_v1.h \
  /home/michael/kernel/Kernel/include/linux/dqblk_v2.h \
  /home/michael/kernel/Kernel/include/linux/dqblk_qtree.h \
  /home/michael/kernel/Kernel/include/linux/nfs_fs_i.h \
  /home/michael/kernel/Kernel/include/linux/fcntl.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/fcntl.h \
  /home/michael/kernel/Kernel/include/asm-generic/fcntl.h \
  /home/michael/kernel/Kernel/include/linux/err.h \
  /home/michael/kernel/Kernel/include/linux/highmem.h \
    $(wildcard include/config/arch/want/kmap/atomic/flush.h) \
    $(wildcard include/config/x86/32.h) \
    $(wildcard include/config/debug/highmem.h) \
  /home/michael/kernel/Kernel/include/linux/uaccess.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/uaccess.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/unified.h \
    $(wildcard include/config/arm/asm/unified.h) \
  /home/michael/kernel/Kernel/include/linux/hardirq.h \
  /home/michael/kernel/Kernel/include/linux/ftrace_irq.h \
    $(wildcard include/config/ftrace/nmi/enter.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hardirq.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/irq.h \
    $(wildcard include/config/sparse/irq.h) \
  /home/michael/kernel/Kernel/include/linux/irq_cpustat.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/cacheflush.h \
    $(wildcard include/config/smp/on/up.h) \
    $(wildcard include/config/arm/errata/411920.h) \
    $(wildcard include/config/free/pages/rdonly.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/glue-cache.h \
    $(wildcard include/config/cpu/cache/v3.h) \
    $(wildcard include/config/cpu/cache/v4.h) \
    $(wildcard include/config/cpu/cache/v4wb.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/shmparam.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/cachetype.h \
    $(wildcard include/config/cpu/cache/vivt.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/kmap_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/highmem.h \
    $(wildcard include/config/cpu/tlb/v6.h) \
  /home/michael/kernel/Kernel/include/linux/hugetlb_inline.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bugs.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/tlbflush.h \
    $(wildcard include/config/cpu/tlb/v3.h) \
    $(wildcard include/config/cpu/tlb/v4wt.h) \
    $(wildcard include/config/cpu/tlb/fa.h) \
    $(wildcard include/config/cpu/tlb/v4wbi.h) \
    $(wildcard include/config/cpu/tlb/feroceon.h) \
    $(wildcard include/config/cpu/tlb/v4wb.h) \
    $(wildcard include/config/cpu/tlb/v7.h) \
    $(wildcard include/config/arm/errata/720789.h) \
  /home/michael/kernel/Kernel/arch/arm/mm/mm.h \

arch/arm/mm/fault-armv.o: $(deps_arch/arm/mm/fault-armv.o)

$(deps_arch/arm/mm/fault-armv.o):
