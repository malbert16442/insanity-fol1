cmd_kernel/range.o := /home/michael/kernel/Kernel/scripts/gcc-wrapper.py /home/michael/kernel/toolchain/arm-eabi-4.8/bin/arm-eabi-gcc -Wp,-MD,kernel/.range.o.d  -nostdinc -isystem /home/michael/kernel/toolchain/arm-eabi-4.8/bin/../lib/gcc/arm-eabi/4.8.5/include -I/home/michael/kernel/Kernel/arch/arm/include -Iarch/arm/include/generated -Iinclude  -I/home/michael/kernel/Kernel/include -include /home/michael/kernel/Kernel/include/linux/kconfig.h  -I/home/michael/kernel/Kernel/kernel -Ikernel -D__KERNEL__ -mlittle-endian   -I/home/michael/kernel/Kernel/arch/arm/mach-msm/include -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -Wno-maybe-uninitialized -marm -fno-dwarf2-cfi-asm -fstack-protector -mabi=aapcs-linux -mno-thumb-interwork -funwind-tables -D__LINUX_ARM_ARCH__=7 -mcpu=cortex-a15 -msoft-float -Uarm -Wframe-larger-than=1024 -Wno-unused-but-set-variable -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(range)"  -D"KBUILD_MODNAME=KBUILD_STR(range)" -c -o kernel/range.o /home/michael/kernel/Kernel/kernel/range.c

source_kernel/range.o := /home/michael/kernel/Kernel/kernel/range.c

deps_kernel/range.o := \
  /home/michael/kernel/Kernel/include/linux/kernel.h \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/ring/buffer.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/compaction.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
  /home/michael/kernel/Kernel/include/linux/sysinfo.h \
  /home/michael/kernel/Kernel/include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/types.h \
  /home/michael/kernel/Kernel/include/asm-generic/int-ll64.h \
  arch/arm/include/generated/asm/bitsperlong.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitsperlong.h \
  /home/michael/kernel/Kernel/include/linux/posix_types.h \
  /home/michael/kernel/Kernel/include/linux/stddef.h \
  /home/michael/kernel/Kernel/include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc4.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/posix_types.h \
  /home/michael/kernel/Kernel/include/asm-generic/posix_types.h \
  /home/michael/kernel/toolchain/arm-eabi-4.8/lib/gcc/arm-eabi/4.8.5/include/stdarg.h \
  /home/michael/kernel/Kernel/include/linux/linkage.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/linkage.h \
  /home/michael/kernel/Kernel/include/linux/bitops.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bitops.h \
    $(wildcard include/config/smp.h) \
  /home/michael/kernel/Kernel/include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /home/michael/kernel/Kernel/include/linux/typecheck.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/irqflags.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/cpu/endian/be8.h) \
    $(wildcard include/config/arm/thumb.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hwcap.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/non-atomic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/fls64.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/sched.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/arch_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/const_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/lock.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/le.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/byteorder.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/little_endian.h \
  /home/michael/kernel/Kernel/include/linux/swab.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/swab.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/generic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /home/michael/kernel/Kernel/include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  /home/michael/kernel/Kernel/include/linux/printk.h \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
    $(wildcard include/config/gsm/modem/sprd6500.h) \
  /home/michael/kernel/Kernel/include/linux/init.h \
    $(wildcard include/config/deferred/initcalls.h) \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/hotplug.h) \
  /home/michael/kernel/Kernel/include/linux/dynamic_debug.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/div64.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/compiler.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/thumb2/kernel.h) \
    $(wildcard include/config/debug/bugverbose.h) \
    $(wildcard include/config/arm/lpae.h) \
  /home/michael/kernel/Kernel/include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /home/michael/kernel/Kernel/include/linux/sort.h \
  /home/michael/kernel/Kernel/include/linux/range.h \

kernel/range.o: $(deps_kernel/range.o)

$(deps_kernel/range.o):
