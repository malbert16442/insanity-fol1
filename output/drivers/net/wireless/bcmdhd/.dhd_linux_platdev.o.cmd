cmd_drivers/net/wireless/bcmdhd/dhd_linux_platdev.o := /home/michael/kernel/Kernel/scripts/gcc-wrapper.py /home/michael/kernel/toolchain/arm-eabi-4.8/bin/arm-eabi-gcc -Wp,-MD,drivers/net/wireless/bcmdhd/.dhd_linux_platdev.o.d  -nostdinc -isystem /home/michael/kernel/toolchain/arm-eabi-4.8/bin/../lib/gcc/arm-eabi/4.8.5/include -I/home/michael/kernel/Kernel/arch/arm/include -Iarch/arm/include/generated -Iinclude  -I/home/michael/kernel/Kernel/include -include /home/michael/kernel/Kernel/include/linux/kconfig.h  -I/home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd -Idrivers/net/wireless/bcmdhd -D__KERNEL__ -mlittle-endian   -I/home/michael/kernel/Kernel/arch/arm/mach-msm/include -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -Wno-maybe-uninitialized -marm -fno-dwarf2-cfi-asm -fstack-protector -mabi=aapcs-linux -mno-thumb-interwork -funwind-tables -D__LINUX_ARM_ARCH__=7 -mcpu=cortex-a15 -msoft-float -Uarm -Wframe-larger-than=1024 -Wno-unused-but-set-variable -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO   -I/include   -I/home/michael/kernel/Kernel/output -Wall -Wstrict-prototypes -Dlinux -DLINUX -DBCMDRIVER -DBCMDONGLEHOST -DUNRELEASEDCHIP -DBCMDMA32 -DBCMFILEIMAGE -DDHDTHREAD -DBDC -DOOB_INTR_ONLY -DDHD_BCMEVENTS -DSHOW_EVENTS -DBCMDBG -DMMC_SDIO_ABORT -DBCMSDIO -DBCMLXSDMMC -DWLP2P -DWIFI_ACT_FRAME -DARP_OFFLOAD_SUPPORT -DKEEP_ALIVE -DCSCAN -DPKT_FILTER_SUPPORT -DEMBEDDED_PLATFORM -DPNO_SUPPORT -DCUSTOMER_HW4 -DWL_CFG80211 -DSIMPLE_MAC_PRINT -DDEBUGFS_CFG80211 -DBCMASSERT_LOG -DDHD_8021X_DUMP -DVSDB -DPROP_TXSTATUS -DWL_CFG80211_VSDB_PRIORITIZE_SCAN_REQUEST -DWL_SCB_TIMEOUT=10 -DCUSTOM_TDLS_IDLE_MODE_SETTING=10000 -DCUSTOM_TDLS_RSSI_THRESHOLD_HIGH=-80 -DCUSTOM_TDLS_RSSI_THRESHOLD_LOW=-85 -DROAM_AP_ENV_DETECTION -DROAM_ENABLE -DROAM_CHANNEL_CACHE -DROAM_API -DENABLE_FW_ROAM_SUSPEND -DWL_SUPPORT_AUTO_CHANNEL -DSUPPORT_HIDDEN_AP -DSUPPORT_SOFTAP_SINGL_DISASSOC -DDISABLE_11H_SOFTAP -DSUPPORT_PM2_ONLY -DSUPPORT_DEEP_SLEEP -DSUPPORT_AMPDU_MPDU_CMD -DBLOCK_IPV6_PACKET -DPASS_IPV4_SUSPEND -DSOFTAP_SEND_HANGEVT -DCUSTOM_PNO_EVENT_LOCK_xTIME=10 -DPASS_ALL_MCAST_PKTS -DDHD_USE_EARLYSUSPEND -DWIFI_TURNOFF_DELAY=100 -DESCAN_RESULT_PATCH -DDUAL_ESCAN_RESULT_BUFFER -DESCAN_BUF_OVERFLOW_MGMT -DCONFIG_DHD_USE_STATIC_BUF -DENHANCED_STATIC_BUF -DSTATIC_WL_PRIV_STRUCT -DCUSTOM_SUSPEND_BCN_LI_DTIM=0 -DIOCTL_RESP_TIMEOUT=5000 -DCUSTOM_DPC_PRIO_SETTING=98 -DPKTPRIO_OVERRIDE -DWAIT_DEQUEUE -DCONFIG_CONTROL_PM -DCONFIG_HAS_WAKELOCK -DDHD_USE_IDLECOUNT -DUSE_INITIAL_SHORT_DWELL_TIME -DDHDENABLE_TAILPAD -DSUPPORT_P2P_GO_PS -DWL_ENABLE_P2P_IF -DWL_SUPPORT_BACKPORTED_KPATCHES -DWL_CFG80211_STA_EVENT -DWL_IFACE_COMB_NUM_CHANNELS -DWL_NEWCFG_PRIVCMD_SUPPORT -DBCM4354_CHIP -DHW_OOB -DSUPPORT_MULTIPLE_REVISION -DMIMO_ANT_SETTING -DUSE_CID_CHECK -DENABLE_BCN_LI_BCN_WAKEUP -DSDIO_CRC_ERROR_FIX -DCUSTOM_GLOM_SETTING=8 -DCUSTOM_RXCHAIN=1 -DUSE_DYNAMIC_F2_BLKSIZE -DDYNAMIC_F2_BLKSIZE_FOR_NONLEGACY=128 -DBCMSDIOH_TXGLOM -DCUSTOM_TXGLOM=1 -DBCMSDIOH_TXGLOM_HIGHSPEED -DDHDTCPACK_SUPPRESS -DUSE_WL_TXBF -DUSE_WL_FRAMEBURST -DRXFRAME_THREAD -DREPEAT_READFRAME -DCUSTOM_AMPDU_BA_WSIZE=64 -DCUSTOM_IBSS_AMPDU_BA_WSIZE=16 -DCUSTOM_DPC_CPUCORE=0 -DPROP_TXSTATUS_VSDB -DCUSTOM_DEF_TXGLOM_SIZE=40 -DDHD_TXBOUND=40 -DENABLE_ADAPTIVE_SCHED -DCUSTOM_CPUFREQ_THRESH=1000000 -DCUSTOM_MAX_TXGLOM_SIZE=40 -DMAX_HDR_READ=128 -DDHD_FIRSTREAD=128 -DCUSTOM_AMPDU_MPDU=16 -DWL11U -DBCMCCX -DWES_SUPPORT -DOKC_SUPPORT -DWLTDLS -DWLFBT -DDHD_ENABLE_LPC -DWLAIBSS -DSUPPORT_LTECX -DSUPPORT_2G_VHT -DSUPPORT_WL_TXPOWER -DTEST_TX_POWER_CONTROL -DENABLE_INSMOD_NO_FW_LOAD -DUSE_LATE_INITCALL_SYNC -DCUSTOM_PSPRETEND_THR=30 -include "dhd_sec_feature.h" -DDHD_DEBUG -DSRCBASE=\"drivers/net/wireless/bcmdhd\"  -I/home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/ -Idrivers/net/wireless/bcmdhd/include/  -I/home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/ -Idrivers/net/wireless/bcmdhd/    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(dhd_linux_platdev)"  -D"KBUILD_MODNAME=KBUILD_STR(dhd)" -c -o drivers/net/wireless/bcmdhd/dhd_linux_platdev.o /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd_linux_platdev.c

source_drivers/net/wireless/bcmdhd/dhd_linux_platdev.o := /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd_linux_platdev.c

deps_drivers/net/wireless/bcmdhd/dhd_linux_platdev.o := \
    $(wildcard include/config/wifi/control/func.h) \
    $(wildcard include/config/of.h) \
    $(wildcard include/config/arch/msm.h) \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd_sec_feature.h \
    $(wildcard include/config/wifi/broadcom/cob.h) \
    $(wildcard include/config/bcm4334.h) \
    $(wildcard include/config/mach/vienna.h) \
    $(wildcard include/config/mach/v2.h) \
    $(wildcard include/config/mach/lt03eur.h) \
    $(wildcard include/config/mach/lt03skt.h) \
    $(wildcard include/config/mach/lt03ktt.h) \
    $(wildcard include/config/mach/lt03lgt.h) \
    $(wildcard include/config/mach/chagall.h) \
    $(wildcard include/config/mach/klimt.h) \
    $(wildcard include/config/mach/chagall/kdi.h) \
    $(wildcard include/config/wlan/region/code.h) \
    $(wildcard include/config/mach/universal5410.h) \
    $(wildcard include/config/sec/k/project.h) \
    $(wildcard include/config/sec/kactive/project.h) \
    $(wildcard include/config/sec/ksports/project.h) \
    $(wildcard include/config/mach/klte/dcm.h) \
    $(wildcard include/config/mach/kactivelte/dcm.h) \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/linuxver.h \
    $(wildcard include/config/net/radio.h) \
    $(wildcard include/config/wireless/ext.h) \
    $(wildcard include/config/rfkill.h) \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/typedefs.h \
  include/linux/version.h \
  /home/michael/kernel/Kernel/include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/types.h \
  /home/michael/kernel/Kernel/include/asm-generic/int-ll64.h \
  arch/arm/include/generated/asm/bitsperlong.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitsperlong.h \
  /home/michael/kernel/Kernel/include/linux/posix_types.h \
  /home/michael/kernel/Kernel/include/linux/stddef.h \
  /home/michael/kernel/Kernel/include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc4.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/posix_types.h \
  /home/michael/kernel/Kernel/include/asm-generic/posix_types.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/bcmdefs.h \
  /home/michael/kernel/Kernel/include/linux/module.h \
    $(wildcard include/config/sysfs.h) \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/unused/symbols.h) \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/kallsyms.h) \
    $(wildcard include/config/smp.h) \
    $(wildcard include/config/tracepoints.h) \
    $(wildcard include/config/tracing.h) \
    $(wildcard include/config/event/tracing.h) \
    $(wildcard include/config/ftrace/mcount/record.h) \
    $(wildcard include/config/module/unload.h) \
    $(wildcard include/config/constructors.h) \
    $(wildcard include/config/debug/set/module/ronx.h) \
  /home/michael/kernel/Kernel/include/linux/list.h \
    $(wildcard include/config/debug/list.h) \
  /home/michael/kernel/Kernel/include/linux/poison.h \
    $(wildcard include/config/illegal/pointer/value.h) \
  /home/michael/kernel/Kernel/include/linux/const.h \
  /home/michael/kernel/Kernel/include/linux/stat.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/stat.h \
  /home/michael/kernel/Kernel/include/linux/time.h \
    $(wildcard include/config/arch/uses/gettimeoffset.h) \
  /home/michael/kernel/Kernel/include/linux/cache.h \
    $(wildcard include/config/arch/has/cache/line/size.h) \
  /home/michael/kernel/Kernel/include/linux/kernel.h \
    $(wildcard include/config/preempt/voluntary.h) \
    $(wildcard include/config/debug/atomic/sleep.h) \
    $(wildcard include/config/prove/locking.h) \
    $(wildcard include/config/ring/buffer.h) \
    $(wildcard include/config/numa.h) \
    $(wildcard include/config/compaction.h) \
  /home/michael/kernel/Kernel/include/linux/sysinfo.h \
  /home/michael/kernel/toolchain/arm-eabi-4.8/lib/gcc/arm-eabi/4.8.5/include/stdarg.h \
  /home/michael/kernel/Kernel/include/linux/linkage.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/linkage.h \
  /home/michael/kernel/Kernel/include/linux/bitops.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bitops.h \
  /home/michael/kernel/Kernel/include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /home/michael/kernel/Kernel/include/linux/typecheck.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/irqflags.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/cpu/endian/be8.h) \
    $(wildcard include/config/arm/thumb.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hwcap.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/non-atomic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/fls64.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/sched.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/arch_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/const_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/lock.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/le.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/byteorder.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/little_endian.h \
  /home/michael/kernel/Kernel/include/linux/swab.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/swab.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/generic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /home/michael/kernel/Kernel/include/linux/log2.h \
    $(wildcard include/config/arch/has/ilog2/u32.h) \
    $(wildcard include/config/arch/has/ilog2/u64.h) \
  /home/michael/kernel/Kernel/include/linux/printk.h \
    $(wildcard include/config/printk.h) \
    $(wildcard include/config/dynamic/debug.h) \
    $(wildcard include/config/gsm/modem/sprd6500.h) \
  /home/michael/kernel/Kernel/include/linux/init.h \
    $(wildcard include/config/deferred/initcalls.h) \
    $(wildcard include/config/hotplug.h) \
  /home/michael/kernel/Kernel/include/linux/dynamic_debug.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/div64.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/compiler.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/thumb2/kernel.h) \
    $(wildcard include/config/debug/bugverbose.h) \
    $(wildcard include/config/arm/lpae.h) \
  /home/michael/kernel/Kernel/include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/cache.h \
    $(wildcard include/config/arm/l1/cache/shift.h) \
    $(wildcard include/config/aeabi.h) \
  /home/michael/kernel/Kernel/include/linux/seqlock.h \
  /home/michael/kernel/Kernel/include/linux/spinlock.h \
    $(wildcard include/config/debug/spinlock.h) \
    $(wildcard include/config/generic/lockbreak.h) \
    $(wildcard include/config/preempt.h) \
    $(wildcard include/config/debug/lock/alloc.h) \
  /home/michael/kernel/Kernel/include/linux/preempt.h \
    $(wildcard include/config/debug/preempt.h) \
    $(wildcard include/config/preempt/count.h) \
    $(wildcard include/config/preempt/notifiers.h) \
  /home/michael/kernel/Kernel/include/linux/thread_info.h \
    $(wildcard include/config/compat.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/thread_info.h \
    $(wildcard include/config/arm/thumbee.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/fpstate.h \
    $(wildcard include/config/vfpv3.h) \
    $(wildcard include/config/iwmmxt.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/domain.h \
    $(wildcard include/config/verify/permission/fault.h) \
    $(wildcard include/config/io/36.h) \
    $(wildcard include/config/cpu/use/domains.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/barrier.h \
    $(wildcard include/config/cpu/32v6k.h) \
    $(wildcard include/config/cpu/xsc3.h) \
    $(wildcard include/config/cpu/fa526.h) \
    $(wildcard include/config/arch/has/barriers.h) \
    $(wildcard include/config/arm/dma/mem/bufferable.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/outercache.h \
    $(wildcard include/config/outer/cache/sync.h) \
    $(wildcard include/config/outer/cache.h) \
  /home/michael/kernel/Kernel/include/linux/stringify.h \
  /home/michael/kernel/Kernel/include/linux/bottom_half.h \
  /home/michael/kernel/Kernel/include/linux/spinlock_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/spinlock_types.h \
  /home/michael/kernel/Kernel/include/linux/lockdep.h \
    $(wildcard include/config/lockdep.h) \
    $(wildcard include/config/lock/stat.h) \
    $(wildcard include/config/prove/rcu.h) \
  /home/michael/kernel/Kernel/include/linux/rwlock_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/spinlock.h \
    $(wildcard include/config/msm/krait/wfe/fixup.h) \
    $(wildcard include/config/arm/ticket/locks.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/processor.h \
    $(wildcard include/config/have/hw/breakpoint.h) \
    $(wildcard include/config/mmu.h) \
    $(wildcard include/config/arm/errata/754327.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hw_breakpoint.h \
  /home/michael/kernel/Kernel/include/linux/rwlock.h \
  /home/michael/kernel/Kernel/include/linux/spinlock_api_smp.h \
    $(wildcard include/config/inline/spin/lock.h) \
    $(wildcard include/config/inline/spin/lock/bh.h) \
    $(wildcard include/config/inline/spin/lock/irq.h) \
    $(wildcard include/config/inline/spin/lock/irqsave.h) \
    $(wildcard include/config/inline/spin/trylock.h) \
    $(wildcard include/config/inline/spin/trylock/bh.h) \
    $(wildcard include/config/uninline/spin/unlock.h) \
    $(wildcard include/config/inline/spin/unlock/bh.h) \
    $(wildcard include/config/inline/spin/unlock/irq.h) \
    $(wildcard include/config/inline/spin/unlock/irqrestore.h) \
  /home/michael/kernel/Kernel/include/linux/rwlock_api_smp.h \
    $(wildcard include/config/inline/read/lock.h) \
    $(wildcard include/config/inline/write/lock.h) \
    $(wildcard include/config/inline/read/lock/bh.h) \
    $(wildcard include/config/inline/write/lock/bh.h) \
    $(wildcard include/config/inline/read/lock/irq.h) \
    $(wildcard include/config/inline/write/lock/irq.h) \
    $(wildcard include/config/inline/read/lock/irqsave.h) \
    $(wildcard include/config/inline/write/lock/irqsave.h) \
    $(wildcard include/config/inline/read/trylock.h) \
    $(wildcard include/config/inline/write/trylock.h) \
    $(wildcard include/config/inline/read/unlock.h) \
    $(wildcard include/config/inline/write/unlock.h) \
    $(wildcard include/config/inline/read/unlock/bh.h) \
    $(wildcard include/config/inline/write/unlock/bh.h) \
    $(wildcard include/config/inline/read/unlock/irq.h) \
    $(wildcard include/config/inline/write/unlock/irq.h) \
    $(wildcard include/config/inline/read/unlock/irqrestore.h) \
    $(wildcard include/config/inline/write/unlock/irqrestore.h) \
  /home/michael/kernel/Kernel/include/linux/atomic.h \
    $(wildcard include/config/arch/has/atomic/or.h) \
    $(wildcard include/config/generic/atomic64.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/atomic.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/cmpxchg.h \
    $(wildcard include/config/cpu/sa1100.h) \
    $(wildcard include/config/cpu/sa110.h) \
    $(wildcard include/config/cpu/v6.h) \
  /home/michael/kernel/Kernel/include/asm-generic/cmpxchg-local.h \
  /home/michael/kernel/Kernel/include/asm-generic/atomic-long.h \
  /home/michael/kernel/Kernel/include/linux/math64.h \
  /home/michael/kernel/Kernel/include/linux/kmod.h \
  /home/michael/kernel/Kernel/include/linux/gfp.h \
    $(wildcard include/config/kmemcheck.h) \
    $(wildcard include/config/cma.h) \
    $(wildcard include/config/highmem.h) \
    $(wildcard include/config/zone/dma.h) \
    $(wildcard include/config/zone/dma32.h) \
    $(wildcard include/config/pm/sleep.h) \
  /home/michael/kernel/Kernel/include/linux/mmzone.h \
    $(wildcard include/config/force/max/zoneorder.h) \
    $(wildcard include/config/cma/page/counting.h) \
    $(wildcard include/config/memory/hotplug.h) \
    $(wildcard include/config/sparsemem.h) \
    $(wildcard include/config/have/memblock/node/map.h) \
    $(wildcard include/config/discontigmem.h) \
    $(wildcard include/config/flat/node/mem/map.h) \
    $(wildcard include/config/cgroup/mem/res/ctlr.h) \
    $(wildcard include/config/no/bootmem.h) \
    $(wildcard include/config/have/memory/present.h) \
    $(wildcard include/config/have/memoryless/nodes.h) \
    $(wildcard include/config/need/node/memmap/size.h) \
    $(wildcard include/config/have/memblock/node.h) \
    $(wildcard include/config/need/multiple/nodes.h) \
    $(wildcard include/config/have/arch/early/pfn/to/nid.h) \
    $(wildcard include/config/flatmem.h) \
    $(wildcard include/config/sparsemem/extreme.h) \
    $(wildcard include/config/have/arch/pfn/valid.h) \
    $(wildcard include/config/nodes/span/other/nodes.h) \
    $(wildcard include/config/holes/in/zone.h) \
    $(wildcard include/config/arch/has/holes/memorymodel.h) \
  /home/michael/kernel/Kernel/include/linux/wait.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/current.h \
  /home/michael/kernel/Kernel/include/linux/threads.h \
    $(wildcard include/config/nr/cpus.h) \
    $(wildcard include/config/base/small.h) \
  /home/michael/kernel/Kernel/include/linux/numa.h \
    $(wildcard include/config/nodes/shift.h) \
  /home/michael/kernel/Kernel/include/linux/nodemask.h \
  /home/michael/kernel/Kernel/include/linux/bitmap.h \
  /home/michael/kernel/Kernel/include/linux/string.h \
    $(wildcard include/config/binary/printf.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/string.h \
  /home/michael/kernel/Kernel/include/linux/pageblock-flags.h \
    $(wildcard include/config/hugetlb/page.h) \
    $(wildcard include/config/hugetlb/page/size/variable.h) \
  include/generated/bounds.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/page.h \
    $(wildcard include/config/cpu/copy/v3.h) \
    $(wildcard include/config/cpu/copy/v4wt.h) \
    $(wildcard include/config/cpu/copy/v4wb.h) \
    $(wildcard include/config/cpu/copy/feroceon.h) \
    $(wildcard include/config/cpu/copy/fa.h) \
    $(wildcard include/config/cpu/xscale.h) \
    $(wildcard include/config/cpu/copy/v6.h) \
    $(wildcard include/config/kuser/helpers.h) \
    $(wildcard include/config/memory/hotplug/sparse.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/glue.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-2level-types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/memory.h \
    $(wildcard include/config/need/mach/memory/h.h) \
    $(wildcard include/config/page/offset.h) \
    $(wildcard include/config/dram/size.h) \
    $(wildcard include/config/dram/base.h) \
    $(wildcard include/config/have/tcm.h) \
    $(wildcard include/config/arm/patch/phys/virt.h) \
    $(wildcard include/config/phys/offset.h) \
  arch/arm/include/generated/asm/sizes.h \
  /home/michael/kernel/Kernel/include/asm-generic/sizes.h \
  /home/michael/kernel/Kernel/arch/arm/mach-msm/include/mach/memory.h \
    $(wildcard include/config/arch/msm7x30.h) \
    $(wildcard include/config/vmsplit/3g.h) \
    $(wildcard include/config/arch/msm/arm11.h) \
    $(wildcard include/config/arch/msm/cortex/a5.h) \
    $(wildcard include/config/cache/l2x0.h) \
    $(wildcard include/config/arch/msm8x60.h) \
    $(wildcard include/config/arch/msm8960.h) \
    $(wildcard include/config/dont/map/hole/after/membank0.h) \
    $(wildcard include/config/arch/msm/scorpion.h) \
    $(wildcard include/config/arch/msm/krait.h) \
    $(wildcard include/config/arch/msm7x27.h) \
  /home/michael/kernel/Kernel/include/asm-generic/memory_model.h \
    $(wildcard include/config/sparsemem/vmemmap.h) \
  /home/michael/kernel/Kernel/include/asm-generic/getorder.h \
  /home/michael/kernel/Kernel/include/linux/memory_hotplug.h \
    $(wildcard include/config/memory/hotremove.h) \
    $(wildcard include/config/have/arch/nodedata/extension.h) \
  /home/michael/kernel/Kernel/include/linux/notifier.h \
  /home/michael/kernel/Kernel/include/linux/errno.h \
  arch/arm/include/generated/asm/errno.h \
  /home/michael/kernel/Kernel/include/asm-generic/errno.h \
  /home/michael/kernel/Kernel/include/asm-generic/errno-base.h \
  /home/michael/kernel/Kernel/include/linux/mutex.h \
    $(wildcard include/config/debug/mutexes.h) \
    $(wildcard include/config/arch/msm8610.h) \
    $(wildcard include/config/have/arch/mutex/cpu/relax.h) \
  /home/michael/kernel/Kernel/include/linux/rwsem.h \
    $(wildcard include/config/rwsem/generic/spinlock.h) \
  /home/michael/kernel/Kernel/include/linux/rwsem-spinlock.h \
    $(wildcard include/config/sec/forkhang/debug.h) \
  /home/michael/kernel/Kernel/include/linux/srcu.h \
  /home/michael/kernel/Kernel/include/linux/rcupdate.h \
    $(wildcard include/config/rcu/torture/test.h) \
    $(wildcard include/config/tree/rcu.h) \
    $(wildcard include/config/tree/preempt/rcu.h) \
    $(wildcard include/config/rcu/trace.h) \
    $(wildcard include/config/preempt/rcu.h) \
    $(wildcard include/config/tiny/rcu.h) \
    $(wildcard include/config/tiny/preempt/rcu.h) \
    $(wildcard include/config/debug/objects/rcu/head.h) \
    $(wildcard include/config/hotplug/cpu.h) \
    $(wildcard include/config/preempt/rt.h) \
  /home/michael/kernel/Kernel/include/linux/cpumask.h \
    $(wildcard include/config/cpumask/offstack.h) \
    $(wildcard include/config/debug/per/cpu/maps.h) \
    $(wildcard include/config/disable/obsolete/cpumask/functions.h) \
  /home/michael/kernel/Kernel/include/linux/bug.h \
    $(wildcard include/config/panic/on/data/corruption.h) \
  /home/michael/kernel/Kernel/include/linux/completion.h \
  /home/michael/kernel/Kernel/include/linux/debugobjects.h \
    $(wildcard include/config/debug/objects.h) \
    $(wildcard include/config/debug/objects/free.h) \
  /home/michael/kernel/Kernel/include/linux/rcutree.h \
  /home/michael/kernel/Kernel/include/linux/topology.h \
    $(wildcard include/config/sched/smt.h) \
    $(wildcard include/config/sched/mc.h) \
    $(wildcard include/config/sched/book.h) \
    $(wildcard include/config/use/percpu/numa/node/id.h) \
  /home/michael/kernel/Kernel/include/linux/smp.h \
    $(wildcard include/config/use/generic/smp/helpers.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/smp.h \
  /home/michael/kernel/Kernel/include/linux/percpu.h \
    $(wildcard include/config/need/per/cpu/embed/first/chunk.h) \
    $(wildcard include/config/need/per/cpu/page/first/chunk.h) \
    $(wildcard include/config/have/setup/per/cpu/area.h) \
  /home/michael/kernel/Kernel/include/linux/pfn.h \
  arch/arm/include/generated/asm/percpu.h \
  /home/michael/kernel/Kernel/include/asm-generic/percpu.h \
  /home/michael/kernel/Kernel/include/linux/percpu-defs.h \
    $(wildcard include/config/debug/force/weak/per/cpu.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/topology.h \
    $(wildcard include/config/arm/cpu/topology.h) \
  /home/michael/kernel/Kernel/include/asm-generic/topology.h \
  /home/michael/kernel/Kernel/include/linux/mmdebug.h \
    $(wildcard include/config/debug/vm.h) \
    $(wildcard include/config/debug/virtual.h) \
  /home/michael/kernel/Kernel/include/linux/workqueue.h \
    $(wildcard include/config/debug/objects/work.h) \
    $(wildcard include/config/freezer.h) \
    $(wildcard include/config/workqueue/front.h) \
  /home/michael/kernel/Kernel/include/linux/timer.h \
    $(wildcard include/config/timer/stats.h) \
    $(wildcard include/config/debug/objects/timers.h) \
  /home/michael/kernel/Kernel/include/linux/ktime.h \
    $(wildcard include/config/ktime/scalar.h) \
  /home/michael/kernel/Kernel/include/linux/jiffies.h \
  /home/michael/kernel/Kernel/include/linux/timex.h \
  /home/michael/kernel/Kernel/include/linux/param.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/param.h \
    $(wildcard include/config/hz.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/timex.h \
  /home/michael/kernel/Kernel/arch/arm/mach-msm/include/mach/timex.h \
    $(wildcard include/config/have/arch/has/current/timer.h) \
  /home/michael/kernel/Kernel/include/linux/sysctl.h \
    $(wildcard include/config/sysctl.h) \
  /home/michael/kernel/Kernel/include/linux/rbtree.h \
  /home/michael/kernel/Kernel/include/linux/elf.h \
  /home/michael/kernel/Kernel/include/linux/elf-em.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/elf.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/user.h \
  /home/michael/kernel/Kernel/include/linux/kobject.h \
  /home/michael/kernel/Kernel/include/linux/sysfs.h \
  /home/michael/kernel/Kernel/include/linux/kobject_ns.h \
  /home/michael/kernel/Kernel/include/linux/kref.h \
  /home/michael/kernel/Kernel/include/linux/moduleparam.h \
    $(wildcard include/config/alpha.h) \
    $(wildcard include/config/ia64.h) \
    $(wildcard include/config/ppc64.h) \
  /home/michael/kernel/Kernel/include/linux/tracepoint.h \
  /home/michael/kernel/Kernel/include/linux/static_key.h \
  /home/michael/kernel/Kernel/include/linux/jump_label.h \
    $(wildcard include/config/jump/label.h) \
  /home/michael/kernel/Kernel/include/linux/export.h \
    $(wildcard include/config/symbol/prefix.h) \
    $(wildcard include/config/modversions.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/module.h \
    $(wildcard include/config/arm/unwind.h) \
  /home/michael/kernel/Kernel/include/linux/slab.h \
    $(wildcard include/config/slab/debug.h) \
    $(wildcard include/config/failslab.h) \
    $(wildcard include/config/slub.h) \
    $(wildcard include/config/slob.h) \
    $(wildcard include/config/debug/slab.h) \
    $(wildcard include/config/slab.h) \
  /home/michael/kernel/Kernel/include/linux/slub_def.h \
    $(wildcard include/config/slub/stats.h) \
    $(wildcard include/config/slub/debug.h) \
  /home/michael/kernel/Kernel/include/linux/kmemleak.h \
    $(wildcard include/config/debug/kmemleak.h) \
  /home/michael/kernel/Kernel/include/linux/mm.h \
    $(wildcard include/config/fix/movable/zone.h) \
    $(wildcard include/config/stack/growsup.h) \
    $(wildcard include/config/transparent/hugepage.h) \
    $(wildcard include/config/ksm.h) \
    $(wildcard include/config/proc/fs.h) \
    $(wildcard include/config/debug/pagealloc.h) \
    $(wildcard include/config/hibernation.h) \
    $(wildcard include/config/use/user/accessible/timers.h) \
    $(wildcard include/config/hugetlbfs.h) \
  /home/michael/kernel/Kernel/include/linux/prio_tree.h \
  /home/michael/kernel/Kernel/include/linux/debug_locks.h \
    $(wildcard include/config/debug/locking/api/selftests.h) \
  /home/michael/kernel/Kernel/include/linux/mm_types.h \
    $(wildcard include/config/split/ptlock/cpus.h) \
    $(wildcard include/config/want/page/debug/flags.h) \
    $(wildcard include/config/have/aligned/struct/page.h) \
    $(wildcard include/config/aio.h) \
    $(wildcard include/config/mm/owner.h) \
    $(wildcard include/config/mmu/notifier.h) \
  /home/michael/kernel/Kernel/include/linux/auxvec.h \
  arch/arm/include/generated/asm/auxvec.h \
  /home/michael/kernel/Kernel/include/asm-generic/auxvec.h \
  /home/michael/kernel/Kernel/include/linux/page-debug-flags.h \
    $(wildcard include/config/page/poisoning.h) \
    $(wildcard include/config/page/guard.h) \
    $(wildcard include/config/page/debug/something/else.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/mmu.h \
    $(wildcard include/config/cpu/has/asid.h) \
  /home/michael/kernel/Kernel/include/linux/range.h \
  /home/michael/kernel/Kernel/include/linux/bit_spinlock.h \
  /home/michael/kernel/Kernel/include/linux/shrinker.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable.h \
    $(wildcard include/config/highpte.h) \
    $(wildcard include/config/tima/rkp/l2/group.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/proc-fns.h \
    $(wildcard include/config/tima/rkp/l2/tables.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/glue-proc.h \
    $(wildcard include/config/cpu/arm610.h) \
    $(wildcard include/config/cpu/arm7tdmi.h) \
    $(wildcard include/config/cpu/arm710.h) \
    $(wildcard include/config/cpu/arm720t.h) \
    $(wildcard include/config/cpu/arm740t.h) \
    $(wildcard include/config/cpu/arm9tdmi.h) \
    $(wildcard include/config/cpu/arm920t.h) \
    $(wildcard include/config/cpu/arm922t.h) \
    $(wildcard include/config/cpu/arm925t.h) \
    $(wildcard include/config/cpu/arm926t.h) \
    $(wildcard include/config/cpu/arm940t.h) \
    $(wildcard include/config/cpu/arm946e.h) \
    $(wildcard include/config/cpu/arm1020.h) \
    $(wildcard include/config/cpu/arm1020e.h) \
    $(wildcard include/config/cpu/arm1022.h) \
    $(wildcard include/config/cpu/arm1026.h) \
    $(wildcard include/config/cpu/mohawk.h) \
    $(wildcard include/config/cpu/feroceon.h) \
    $(wildcard include/config/cpu/v6k.h) \
    $(wildcard include/config/cpu/v7.h) \
  /home/michael/kernel/Kernel/include/asm-generic/pgtable-nopud.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-hwdef.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-2level-hwdef.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pgtable-2level.h \
    $(wildcard include/config/tima/rkp.h) \
    $(wildcard include/config/tima/rkp/l1/tables.h) \
    $(wildcard include/config/tima/rkp/lazy/mmu.h) \
  /home/michael/kernel/Kernel/include/asm-generic/pgtable.h \
  /home/michael/kernel/Kernel/include/linux/page-flags.h \
    $(wildcard include/config/pageflags/extended.h) \
    $(wildcard include/config/arch/uses/pg/uncached.h) \
    $(wildcard include/config/memory/failure.h) \
    $(wildcard include/config/scfs/lower/pagecache/invalidation.h) \
    $(wildcard include/config/swap.h) \
    $(wildcard include/config/s390.h) \
  /home/michael/kernel/Kernel/include/linux/huge_mm.h \
  /home/michael/kernel/Kernel/include/linux/vmstat.h \
    $(wildcard include/config/vm/event/counters.h) \
  /home/michael/kernel/Kernel/include/linux/vm_event_item.h \
    $(wildcard include/config/migration.h) \
  /home/michael/kernel/Kernel/include/linux/pci.h \
    $(wildcard include/config/pci/iov.h) \
    $(wildcard include/config/pcieaspm.h) \
    $(wildcard include/config/pci/msi.h) \
    $(wildcard include/config/pci/ats.h) \
    $(wildcard include/config/pci.h) \
    $(wildcard include/config/pcieportbus.h) \
    $(wildcard include/config/pcieaer.h) \
    $(wildcard include/config/pcie/ecrc.h) \
    $(wildcard include/config/ht/irq.h) \
    $(wildcard include/config/pci/domains.h) \
    $(wildcard include/config/pci/quirks.h) \
    $(wildcard include/config/pci/mmconfig.h) \
    $(wildcard include/config/hotplug/pci.h) \
    $(wildcard include/config/eeh.h) \
  /home/michael/kernel/Kernel/include/linux/pci_regs.h \
  /home/michael/kernel/Kernel/include/linux/mod_devicetable.h \
    $(wildcard include/config/input/expanded/abs.h) \
  /home/michael/kernel/Kernel/include/linux/ioport.h \
  /home/michael/kernel/Kernel/include/linux/device.h \
    $(wildcard include/config/debug/devres.h) \
    $(wildcard include/config/pinctrl.h) \
    $(wildcard include/config/devtmpfs.h) \
    $(wildcard include/config/sysfs/deprecated.h) \
  /home/michael/kernel/Kernel/include/linux/klist.h \
  /home/michael/kernel/Kernel/include/linux/pinctrl/devinfo.h \
  /home/michael/kernel/Kernel/include/linux/pm.h \
    $(wildcard include/config/pm.h) \
    $(wildcard include/config/pm/runtime.h) \
    $(wildcard include/config/pm/clk.h) \
    $(wildcard include/config/pm/generic/domains.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/device.h \
    $(wildcard include/config/dmabounce.h) \
    $(wildcard include/config/iommu/api.h) \
    $(wildcard include/config/arm/dma/use/iommu.h) \
    $(wildcard include/config/arch/omap.h) \
  /home/michael/kernel/Kernel/include/linux/pm_wakeup.h \
  /home/michael/kernel/Kernel/include/linux/io.h \
    $(wildcard include/config/has/ioport.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/io.h \
    $(wildcard include/config/need/mach/io/h.h) \
    $(wildcard include/config/pcmcia/soc/common.h) \
    $(wildcard include/config/isa.h) \
    $(wildcard include/config/pccard.h) \
  /home/michael/kernel/Kernel/include/asm-generic/pci_iomap.h \
    $(wildcard include/config/no/generic/pci/ioport/map.h) \
    $(wildcard include/config/generic/pci/iomap.h) \
  /home/michael/kernel/Kernel/arch/arm/mach-msm/include/mach/msm_rtb.h \
    $(wildcard include/config/msm/rtb.h) \
  /home/michael/kernel/Kernel/arch/arm/mach-msm/include/mach/io.h \
  /home/michael/kernel/Kernel/include/linux/irqreturn.h \
  /home/michael/kernel/Kernel/include/linux/pci_ids.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/pci.h \
  /home/michael/kernel/Kernel/include/asm-generic/pci-dma-compat.h \
  /home/michael/kernel/Kernel/include/linux/dma-mapping.h \
    $(wildcard include/config/has/dma.h) \
    $(wildcard include/config/arch/has/dma/set/coherent/mask.h) \
    $(wildcard include/config/have/dma/attrs.h) \
    $(wildcard include/config/need/dma/map/state.h) \
  /home/michael/kernel/Kernel/include/linux/err.h \
  /home/michael/kernel/Kernel/include/linux/dma-attrs.h \
  /home/michael/kernel/Kernel/include/linux/dma-direction.h \
  /home/michael/kernel/Kernel/include/linux/scatterlist.h \
    $(wildcard include/config/debug/sg.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/scatterlist.h \
    $(wildcard include/config/arm/has/sg/chain.h) \
  /home/michael/kernel/Kernel/include/asm-generic/scatterlist.h \
    $(wildcard include/config/need/sg/dma/length.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/dma-mapping.h \
  /home/michael/kernel/Kernel/include/linux/dma-debug.h \
    $(wildcard include/config/dma/api/debug.h) \
  /home/michael/kernel/Kernel/include/asm-generic/dma-coherent.h \
    $(wildcard include/config/have/generic/dma/coherent.h) \
  /home/michael/kernel/Kernel/include/asm-generic/dma-mapping-common.h \
  /home/michael/kernel/Kernel/include/linux/kmemcheck.h \
  /home/michael/kernel/Kernel/include/asm-generic/pci-bridge.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/mach/pci.h \
  /home/michael/kernel/Kernel/include/linux/interrupt.h \
    $(wildcard include/config/generic/hardirqs.h) \
    $(wildcard include/config/irq/forced/threading.h) \
    $(wildcard include/config/generic/irq/probe.h) \
  /home/michael/kernel/Kernel/include/linux/irqnr.h \
  /home/michael/kernel/Kernel/include/linux/hardirq.h \
    $(wildcard include/config/virt/cpu/accounting.h) \
    $(wildcard include/config/irq/time/accounting.h) \
  /home/michael/kernel/Kernel/include/linux/ftrace_irq.h \
    $(wildcard include/config/ftrace/nmi/enter.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hardirq.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/irq.h \
    $(wildcard include/config/sparse/irq.h) \
  /home/michael/kernel/Kernel/include/linux/irq_cpustat.h \
  /home/michael/kernel/Kernel/include/linux/hrtimer.h \
    $(wildcard include/config/high/res/timers.h) \
    $(wildcard include/config/timerfd.h) \
  /home/michael/kernel/Kernel/include/linux/timerqueue.h \
  /home/michael/kernel/Kernel/include/linux/kthread.h \
  /home/michael/kernel/Kernel/include/linux/sched.h \
    $(wildcard include/config/runtime/compcache.h) \
    $(wildcard include/config/sched/debug.h) \
    $(wildcard include/config/no/hz.h) \
    $(wildcard include/config/lockup/detector.h) \
    $(wildcard include/config/detect/hung/task.h) \
    $(wildcard include/config/core/dump/default/elf/headers.h) \
    $(wildcard include/config/sched/autogroup.h) \
    $(wildcard include/config/bsd/process/acct.h) \
    $(wildcard include/config/taskstats.h) \
    $(wildcard include/config/audit.h) \
    $(wildcard include/config/cgroups.h) \
    $(wildcard include/config/samp/hotness.h) \
    $(wildcard include/config/inotify/user.h) \
    $(wildcard include/config/fanotify.h) \
    $(wildcard include/config/epoll.h) \
    $(wildcard include/config/posix/mqueue.h) \
    $(wildcard include/config/keys.h) \
    $(wildcard include/config/perf/events.h) \
    $(wildcard include/config/schedstats.h) \
    $(wildcard include/config/task/delay/acct.h) \
    $(wildcard include/config/fair/group/sched.h) \
    $(wildcard include/config/rt/group/sched.h) \
    $(wildcard include/config/blk/dev/io/trace.h) \
    $(wildcard include/config/rcu/boost.h) \
    $(wildcard include/config/compat/brk.h) \
    $(wildcard include/config/cc/stackprotector.h) \
    $(wildcard include/config/sysvipc.h) \
    $(wildcard include/config/auditsyscall.h) \
    $(wildcard include/config/rt/mutexes.h) \
    $(wildcard include/config/block.h) \
    $(wildcard include/config/task/xacct.h) \
    $(wildcard include/config/cpusets.h) \
    $(wildcard include/config/futex.h) \
    $(wildcard include/config/fault/injection.h) \
    $(wildcard include/config/latencytop.h) \
    $(wildcard include/config/function/graph/tracer.h) \
    $(wildcard include/config/sdp.h) \
    $(wildcard include/config/have/unstable/sched/clock.h) \
    $(wildcard include/config/cfs/bandwidth.h) \
    $(wildcard include/config/debug/stack/usage.h) \
    $(wildcard include/config/cgroup/sched.h) \
  /home/michael/kernel/Kernel/include/linux/capability.h \
  arch/arm/include/generated/asm/cputime.h \
  /home/michael/kernel/Kernel/include/asm-generic/cputime.h \
  /home/michael/kernel/Kernel/include/linux/sem.h \
  /home/michael/kernel/Kernel/include/linux/ipc.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/ipcbuf.h \
  /home/michael/kernel/Kernel/include/asm-generic/ipcbuf.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/sembuf.h \
  /home/michael/kernel/Kernel/include/linux/signal.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/signal.h \
  /home/michael/kernel/Kernel/include/asm-generic/signal-defs.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/sigcontext.h \
  arch/arm/include/generated/asm/siginfo.h \
  /home/michael/kernel/Kernel/include/asm-generic/siginfo.h \
  /home/michael/kernel/Kernel/include/linux/pid.h \
  /home/michael/kernel/Kernel/include/linux/proportions.h \
  /home/michael/kernel/Kernel/include/linux/percpu_counter.h \
  /home/michael/kernel/Kernel/include/linux/seccomp.h \
    $(wildcard include/config/seccomp.h) \
  /home/michael/kernel/Kernel/include/linux/rculist.h \
  /home/michael/kernel/Kernel/include/linux/rtmutex.h \
    $(wildcard include/config/debug/rt/mutexes.h) \
  /home/michael/kernel/Kernel/include/linux/plist.h \
    $(wildcard include/config/debug/pi/list.h) \
  /home/michael/kernel/Kernel/include/linux/resource.h \
  arch/arm/include/generated/asm/resource.h \
  /home/michael/kernel/Kernel/include/asm-generic/resource.h \
  /home/michael/kernel/Kernel/include/linux/task_io_accounting.h \
    $(wildcard include/config/task/io/accounting.h) \
  /home/michael/kernel/Kernel/include/linux/latencytop.h \
  /home/michael/kernel/Kernel/include/linux/cred.h \
    $(wildcard include/config/debug/credentials.h) \
    $(wildcard include/config/security.h) \
    $(wildcard include/config/user/ns.h) \
  /home/michael/kernel/Kernel/include/linux/key.h \
  /home/michael/kernel/Kernel/include/linux/selinux.h \
    $(wildcard include/config/security/selinux.h) \
  /home/michael/kernel/Kernel/include/linux/llist.h \
    $(wildcard include/config/arch/have/nmi/safe/cmpxchg.h) \
  /home/michael/kernel/Kernel/include/linux/aio.h \
  /home/michael/kernel/Kernel/include/linux/aio_abi.h \
  /home/michael/kernel/Kernel/include/linux/uio.h \
  /home/michael/kernel/Kernel/include/linux/netdevice.h \
    $(wildcard include/config/dcb.h) \
    $(wildcard include/config/wlan.h) \
    $(wildcard include/config/ax25.h) \
    $(wildcard include/config/mac80211/mesh.h) \
    $(wildcard include/config/tr.h) \
    $(wildcard include/config/net/ipip.h) \
    $(wildcard include/config/net/ipgre.h) \
    $(wildcard include/config/ipv6/sit.h) \
    $(wildcard include/config/ipv6/tunnel.h) \
    $(wildcard include/config/rps.h) \
    $(wildcard include/config/netpoll.h) \
    $(wildcard include/config/xps.h) \
    $(wildcard include/config/bql.h) \
    $(wildcard include/config/rfs/accel.h) \
    $(wildcard include/config/fcoe.h) \
    $(wildcard include/config/net/poll/controller.h) \
    $(wildcard include/config/libfcoe.h) \
    $(wildcard include/config/vlan/8021q.h) \
    $(wildcard include/config/net/dsa.h) \
    $(wildcard include/config/net/ns.h) \
    $(wildcard include/config/netprio/cgroup.h) \
    $(wildcard include/config/net/dsa/tag/dsa.h) \
    $(wildcard include/config/net/dsa/tag/trailer.h) \
    $(wildcard include/config/netpoll/trap.h) \
  /home/michael/kernel/Kernel/include/linux/if.h \
  /home/michael/kernel/Kernel/include/linux/socket.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/socket.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/sockios.h \
  /home/michael/kernel/Kernel/include/linux/sockios.h \
  /home/michael/kernel/Kernel/include/linux/hdlc/ioctl.h \
  /home/michael/kernel/Kernel/include/linux/if_ether.h \
  /home/michael/kernel/Kernel/include/linux/skbuff.h \
    $(wildcard include/config/nf/conntrack.h) \
    $(wildcard include/config/bridge/netfilter.h) \
    $(wildcard include/config/nf/defrag/ipv4.h) \
    $(wildcard include/config/nf/defrag/ipv6.h) \
    $(wildcard include/config/xfrm.h) \
    $(wildcard include/config/net/sched.h) \
    $(wildcard include/config/net/cls/act.h) \
    $(wildcard include/config/ipv6/ndisc/nodetype.h) \
    $(wildcard include/config/net/dma.h) \
    $(wildcard include/config/network/secmark.h) \
    $(wildcard include/config/network/phy/timestamping.h) \
  /home/michael/kernel/Kernel/include/linux/net.h \
  /home/michael/kernel/Kernel/include/linux/random.h \
    $(wildcard include/config/arch/random.h) \
  /home/michael/kernel/Kernel/include/linux/ioctl.h \
  arch/arm/include/generated/asm/ioctl.h \
  /home/michael/kernel/Kernel/include/asm-generic/ioctl.h \
  /home/michael/kernel/Kernel/include/linux/fcntl.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/fcntl.h \
  /home/michael/kernel/Kernel/include/asm-generic/fcntl.h \
  /home/michael/kernel/Kernel/include/linux/textsearch.h \
  /home/michael/kernel/Kernel/include/net/checksum.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/uaccess.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/unified.h \
    $(wildcard include/config/arm/asm/unified.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/checksum.h \
  /home/michael/kernel/Kernel/include/linux/in6.h \
  /home/michael/kernel/Kernel/include/linux/dmaengine.h \
    $(wildcard include/config/async/tx/enable/channel/switch.h) \
    $(wildcard include/config/dma/engine.h) \
    $(wildcard include/config/async/tx/dma.h) \
  /home/michael/kernel/Kernel/include/linux/netdev_features.h \
  /home/michael/kernel/Kernel/include/linux/if_packet.h \
  /home/michael/kernel/Kernel/include/linux/if_link.h \
  /home/michael/kernel/Kernel/include/linux/netlink.h \
  /home/michael/kernel/Kernel/include/linux/pm_qos.h \
  /home/michael/kernel/Kernel/include/linux/miscdevice.h \
  /home/michael/kernel/Kernel/include/linux/major.h \
  /home/michael/kernel/Kernel/include/linux/delay.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/delay.h \
  /home/michael/kernel/Kernel/include/linux/dynamic_queue_limits.h \
  /home/michael/kernel/Kernel/include/linux/ethtool.h \
  /home/michael/kernel/Kernel/include/linux/compat.h \
    $(wildcard include/config/arch/want/old/compat/ipc.h) \
  /home/michael/kernel/Kernel/include/net/net_namespace.h \
    $(wildcard include/config/ipv6.h) \
    $(wildcard include/config/ip/dccp.h) \
    $(wildcard include/config/netfilter.h) \
    $(wildcard include/config/wext/core.h) \
    $(wildcard include/config/net.h) \
  /home/michael/kernel/Kernel/include/net/netns/core.h \
  /home/michael/kernel/Kernel/include/net/netns/mib.h \
    $(wildcard include/config/xfrm/statistics.h) \
  /home/michael/kernel/Kernel/include/net/snmp.h \
  /home/michael/kernel/Kernel/include/linux/snmp.h \
  /home/michael/kernel/Kernel/include/linux/u64_stats_sync.h \
  /home/michael/kernel/Kernel/include/net/netns/unix.h \
  /home/michael/kernel/Kernel/include/net/netns/packet.h \
  /home/michael/kernel/Kernel/include/net/netns/ipv4.h \
    $(wildcard include/config/ip/multiple/tables.h) \
    $(wildcard include/config/ip/mroute.h) \
    $(wildcard include/config/ip/mroute/multiple/tables.h) \
  /home/michael/kernel/Kernel/include/net/inet_frag.h \
  /home/michael/kernel/Kernel/include/net/netns/ipv6.h \
    $(wildcard include/config/ipv6/multiple/tables.h) \
    $(wildcard include/config/ipv6/mroute.h) \
    $(wildcard include/config/ipv6/mroute/multiple/tables.h) \
  /home/michael/kernel/Kernel/include/net/dst_ops.h \
  /home/michael/kernel/Kernel/include/net/netns/dccp.h \
  /home/michael/kernel/Kernel/include/net/netns/x_tables.h \
    $(wildcard include/config/bridge/nf/ebtables.h) \
  /home/michael/kernel/Kernel/include/linux/netfilter.h \
    $(wildcard include/config/nf/nat/needed.h) \
  /home/michael/kernel/Kernel/include/linux/in.h \
  /home/michael/kernel/Kernel/include/net/flow.h \
  /home/michael/kernel/Kernel/include/linux/proc_fs.h \
    $(wildcard include/config/proc/devicetree.h) \
    $(wildcard include/config/proc/kcore.h) \
  /home/michael/kernel/Kernel/include/linux/fs.h \
    $(wildcard include/config/fs/posix/acl.h) \
    $(wildcard include/config/quota.h) \
    $(wildcard include/config/fsnotify.h) \
    $(wildcard include/config/ima.h) \
    $(wildcard include/config/debug/writecount.h) \
    $(wildcard include/config/file/locking.h) \
    $(wildcard include/config/fs/xip.h) \
  /home/michael/kernel/Kernel/include/linux/limits.h \
  /home/michael/kernel/Kernel/include/linux/blk_types.h \
    $(wildcard include/config/blk/dev/integrity.h) \
  /home/michael/kernel/Kernel/include/linux/kdev_t.h \
  /home/michael/kernel/Kernel/include/linux/dcache.h \
  /home/michael/kernel/Kernel/include/linux/rculist_bl.h \
  /home/michael/kernel/Kernel/include/linux/list_bl.h \
  /home/michael/kernel/Kernel/include/linux/path.h \
  /home/michael/kernel/Kernel/include/linux/radix-tree.h \
  /home/michael/kernel/Kernel/include/linux/semaphore.h \
  /home/michael/kernel/Kernel/include/linux/fiemap.h \
  /home/michael/kernel/Kernel/include/linux/migrate_mode.h \
  /home/michael/kernel/Kernel/include/linux/quota.h \
    $(wildcard include/config/quota/netlink/interface.h) \
  /home/michael/kernel/Kernel/include/linux/dqblk_xfs.h \
  /home/michael/kernel/Kernel/include/linux/dqblk_v1.h \
  /home/michael/kernel/Kernel/include/linux/dqblk_v2.h \
  /home/michael/kernel/Kernel/include/linux/dqblk_qtree.h \
  /home/michael/kernel/Kernel/include/linux/nfs_fs_i.h \
  /home/michael/kernel/Kernel/include/linux/magic.h \
  /home/michael/kernel/Kernel/include/net/netns/conntrack.h \
  /home/michael/kernel/Kernel/include/linux/list_nulls.h \
  /home/michael/kernel/Kernel/include/net/netns/xfrm.h \
  /home/michael/kernel/Kernel/include/linux/xfrm.h \
  /home/michael/kernel/Kernel/include/linux/seq_file_net.h \
  /home/michael/kernel/Kernel/include/linux/seq_file.h \
  /home/michael/kernel/Kernel/include/net/dsa.h \
  /home/michael/kernel/Kernel/include/net/netprio_cgroup.h \
  /home/michael/kernel/Kernel/include/linux/cgroup.h \
  /home/michael/kernel/Kernel/include/linux/cgroupstats.h \
  /home/michael/kernel/Kernel/include/linux/taskstats.h \
  /home/michael/kernel/Kernel/include/linux/prio_heap.h \
  /home/michael/kernel/Kernel/include/linux/idr.h \
  /home/michael/kernel/Kernel/include/linux/cgroup_subsys.h \
    $(wildcard include/config/cgroup/debug.h) \
    $(wildcard include/config/cgroup/cpuacct.h) \
    $(wildcard include/config/cgroup/device.h) \
    $(wildcard include/config/cgroup/freezer.h) \
    $(wildcard include/config/net/cls/cgroup.h) \
    $(wildcard include/config/blk/cgroup.h) \
    $(wildcard include/config/cgroup/perf.h) \
  /home/michael/kernel/Kernel/include/net/lib80211.h \
  /home/michael/kernel/Kernel/include/linux/ieee80211.h \
  /home/michael/kernel/Kernel/include/linux/platform_device.h \
    $(wildcard include/config/suspend.h) \
    $(wildcard include/config/hibernate/callbacks.h) \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/bcmutils.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/osl.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/linux_osl.h \
    $(wildcard include/config/mmc/msm7x00a.h) \
    $(wildcard include/config/dhd/use/static/buf.h) \
    $(wildcard include/config/nf/conntrack/mark.h) \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/bcmsdh.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd_dbg.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/dhdioctl.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/packed_section_start.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/packed_section_end.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dngl_stats.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd.h \
    $(wildcard include/config/has/wakelock.h) \
  /home/michael/kernel/Kernel/include/linux/etherdevice.h \
    $(wildcard include/config/have/efficient/unaligned/access.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/unaligned.h \
  /home/michael/kernel/Kernel/include/linux/unaligned/le_byteshift.h \
  /home/michael/kernel/Kernel/include/linux/unaligned/be_byteshift.h \
  /home/michael/kernel/Kernel/include/linux/unaligned/generic.h \
  /home/michael/kernel/Kernel/include/linux/wakelock.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/wlioctl.h \
    $(wildcard include/config/usbrndis/retail.h) \
    $(wildcard include/config/item.h) \
    $(wildcard include/config/ver/0.h) \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/ethernet.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/bcmip.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/bcmeth.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/bcmevent.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/802.11.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/wpa.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/proto/802.1d.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/bcmwifi_channels.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/bcmwifi_rates.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/devctrl_if/wlioctl_defs.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/bcm_mpool_pub.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/bcmcdc.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/include/wlfc_proto.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd_bus.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/dhd_linux.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/wl_android.h \
  /home/michael/kernel/Kernel/drivers/net/wireless/bcmdhd/wldev_common.h \
  /home/michael/kernel/Kernel/include/linux/wlan_plat.h \
    $(wildcard include/config/bcm4335.h) \

drivers/net/wireless/bcmdhd/dhd_linux_platdev.o: $(deps_drivers/net/wireless/bcmdhd/dhd_linux_platdev.o)

$(deps_drivers/net/wireless/bcmdhd/dhd_linux_platdev.o):
