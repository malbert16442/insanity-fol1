cmd_lib/find_next_bit.o := /home/michael/kernel/Kernel/scripts/gcc-wrapper.py /home/michael/kernel/toolchain/arm-eabi-4.8/bin/arm-eabi-gcc -Wp,-MD,lib/.find_next_bit.o.d  -nostdinc -isystem /home/michael/kernel/toolchain/arm-eabi-4.8/bin/../lib/gcc/arm-eabi/4.8.5/include -I/home/michael/kernel/Kernel/arch/arm/include -Iarch/arm/include/generated -Iinclude  -I/home/michael/kernel/Kernel/include -include /home/michael/kernel/Kernel/include/linux/kconfig.h  -I/home/michael/kernel/Kernel/lib -Ilib -D__KERNEL__ -mlittle-endian   -I/home/michael/kernel/Kernel/arch/arm/mach-msm/include -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -Wno-maybe-uninitialized -marm -fno-dwarf2-cfi-asm -fstack-protector -mabi=aapcs-linux -mno-thumb-interwork -funwind-tables -D__LINUX_ARM_ARCH__=7 -mcpu=cortex-a15 -msoft-float -Uarm -Wframe-larger-than=1024 -Wno-unused-but-set-variable -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(find_next_bit)"  -D"KBUILD_MODNAME=KBUILD_STR(find_next_bit)" -c -o lib/find_next_bit.o /home/michael/kernel/Kernel/lib/find_next_bit.c

source_lib/find_next_bit.o := /home/michael/kernel/Kernel/lib/find_next_bit.c

deps_lib/find_next_bit.o := \
  /home/michael/kernel/Kernel/include/linux/bitops.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/types.h \
  /home/michael/kernel/Kernel/include/asm-generic/int-ll64.h \
  arch/arm/include/generated/asm/bitsperlong.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitsperlong.h \
    $(wildcard include/config/64bit.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bitops.h \
    $(wildcard include/config/smp.h) \
  /home/michael/kernel/Kernel/include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc4.h \
  /home/michael/kernel/Kernel/include/linux/irqflags.h \
    $(wildcard include/config/trace/irqflags.h) \
    $(wildcard include/config/irqsoff/tracer.h) \
    $(wildcard include/config/preempt/tracer.h) \
    $(wildcard include/config/trace/irqflags/support.h) \
  /home/michael/kernel/Kernel/include/linux/typecheck.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/irqflags.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/ptrace.h \
    $(wildcard include/config/cpu/endian/be8.h) \
    $(wildcard include/config/arm/thumb.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/hwcap.h \
  /home/michael/kernel/Kernel/include/linux/stddef.h \
  /home/michael/kernel/Kernel/include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
  /home/michael/kernel/Kernel/include/linux/posix_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/posix_types.h \
  /home/michael/kernel/Kernel/include/asm-generic/posix_types.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/non-atomic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/fls64.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/sched.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/arch_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/const_hweight.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/lock.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/le.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/byteorder.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/little_endian.h \
  /home/michael/kernel/Kernel/include/linux/swab.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/swab.h \
  /home/michael/kernel/Kernel/include/linux/byteorder/generic.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitops/ext2-atomic-setbit.h \
  /home/michael/kernel/Kernel/include/linux/export.h \
    $(wildcard include/config/symbol/prefix.h) \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/modversions.h) \
    $(wildcard include/config/unused/symbols.h) \

lib/find_next_bit.o: $(deps_lib/find_next_bit.o)

$(deps_lib/find_next_bit.o):
