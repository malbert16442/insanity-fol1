cmd_lib/reciprocal_div.o := /home/michael/kernel/Kernel/scripts/gcc-wrapper.py /home/michael/kernel/toolchain/arm-eabi-4.8/bin/arm-eabi-gcc -Wp,-MD,lib/.reciprocal_div.o.d  -nostdinc -isystem /home/michael/kernel/toolchain/arm-eabi-4.8/bin/../lib/gcc/arm-eabi/4.8.5/include -I/home/michael/kernel/Kernel/arch/arm/include -Iarch/arm/include/generated -Iinclude  -I/home/michael/kernel/Kernel/include -include /home/michael/kernel/Kernel/include/linux/kconfig.h  -I/home/michael/kernel/Kernel/lib -Ilib -D__KERNEL__ -mlittle-endian   -I/home/michael/kernel/Kernel/arch/arm/mach-msm/include -Wall -Wundef -Wstrict-prototypes -Wno-trigraphs -fno-strict-aliasing -fno-common -Werror-implicit-function-declaration -Wno-format-security -fno-delete-null-pointer-checks -Os -Wno-maybe-uninitialized -marm -fno-dwarf2-cfi-asm -fstack-protector -mabi=aapcs-linux -mno-thumb-interwork -funwind-tables -D__LINUX_ARM_ARCH__=7 -mcpu=cortex-a15 -msoft-float -Uarm -Wframe-larger-than=1024 -Wno-unused-but-set-variable -fomit-frame-pointer -g -Wdeclaration-after-statement -Wno-pointer-sign -fno-strict-overflow -fconserve-stack -DCC_HAVE_ASM_GOTO    -D"KBUILD_STR(s)=\#s" -D"KBUILD_BASENAME=KBUILD_STR(reciprocal_div)"  -D"KBUILD_MODNAME=KBUILD_STR(reciprocal_div)" -c -o lib/reciprocal_div.o /home/michael/kernel/Kernel/lib/reciprocal_div.c

source_lib/reciprocal_div.o := /home/michael/kernel/Kernel/lib/reciprocal_div.c

deps_lib/reciprocal_div.o := \
  /home/michael/kernel/Kernel/arch/arm/include/asm/div64.h \
  /home/michael/kernel/Kernel/include/linux/types.h \
    $(wildcard include/config/uid16.h) \
    $(wildcard include/config/lbdaf.h) \
    $(wildcard include/config/arch/dma/addr/t/64bit.h) \
    $(wildcard include/config/phys/addr/t/64bit.h) \
    $(wildcard include/config/64bit.h) \
  /home/michael/kernel/Kernel/arch/arm/include/asm/types.h \
  /home/michael/kernel/Kernel/include/asm-generic/int-ll64.h \
  arch/arm/include/generated/asm/bitsperlong.h \
  /home/michael/kernel/Kernel/include/asm-generic/bitsperlong.h \
  /home/michael/kernel/Kernel/include/linux/posix_types.h \
  /home/michael/kernel/Kernel/include/linux/stddef.h \
  /home/michael/kernel/Kernel/include/linux/compiler.h \
    $(wildcard include/config/sparse/rcu/pointer.h) \
    $(wildcard include/config/trace/branch/profiling.h) \
    $(wildcard include/config/profile/all/branches.h) \
    $(wildcard include/config/enable/must/check.h) \
    $(wildcard include/config/enable/warn/deprecated.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc.h \
    $(wildcard include/config/arch/supports/optimized/inlining.h) \
    $(wildcard include/config/optimize/inlining.h) \
  /home/michael/kernel/Kernel/include/linux/compiler-gcc4.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/posix_types.h \
  /home/michael/kernel/Kernel/include/asm-generic/posix_types.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/compiler.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/bug.h \
    $(wildcard include/config/bug.h) \
    $(wildcard include/config/thumb2/kernel.h) \
    $(wildcard include/config/debug/bugverbose.h) \
    $(wildcard include/config/arm/lpae.h) \
  /home/michael/kernel/Kernel/include/linux/linkage.h \
  /home/michael/kernel/Kernel/arch/arm/include/asm/linkage.h \
  /home/michael/kernel/Kernel/include/asm-generic/bug.h \
    $(wildcard include/config/generic/bug.h) \
    $(wildcard include/config/generic/bug/relative/pointers.h) \
    $(wildcard include/config/smp.h) \
  /home/michael/kernel/Kernel/include/linux/reciprocal_div.h \
  /home/michael/kernel/Kernel/include/linux/export.h \
    $(wildcard include/config/symbol/prefix.h) \
    $(wildcard include/config/modules.h) \
    $(wildcard include/config/modversions.h) \
    $(wildcard include/config/unused/symbols.h) \

lib/reciprocal_div.o: $(deps_lib/reciprocal_div.o)

$(deps_lib/reciprocal_div.o):
