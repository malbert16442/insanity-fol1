#!/bin/bash

export ARCH=arm
export CROSS_COMPILE=/home/michael/kernel/toolchain/arm-eabi-4.8/bin/arm-eabi-
mkdir output

make -C $(pwd) O=output VARIANT_DEFCONFIG=msm8974pro_sec_klte_tmo_defconfig msm8974_sec_defconfig SELINUX_DEFCONFIG=selinux_defconfig
make xconfig 
make mrproper
make -C $(pwd) O=output

cp output/arch/arm/boot/Image $(pwd)/arch/arm/boot/zImage
